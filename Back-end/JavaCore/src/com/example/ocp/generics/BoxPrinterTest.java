package com.example.ocp.generics;

public class BoxPrinterTest {
	public static void main(String[] args) {
		BoxPrinter<Integer> boxPrinter = new BoxPrinter<Integer>(10);
		System.out.println(boxPrinter);
		BoxPrinter<String> printer = new BoxPrinter<String>("this is generic data type");
		System.out.println(printer);
		// diamond syntax
		Pair<Integer, String> worldCup = new Pair<>(2018, "Rusia");
		System.out.println("World cup " + worldCup.getFirst() + " in " + worldCup.getSecond());
	}
}

class BoxPrinter<T> {
	private T val;

	public BoxPrinter(T arg) {
		val = arg;
	}

	public String toString() {
		return "[" + val + "]";
	}
}

class Pair<T1, T2> {
	T1 object1;
	T2 object2;

	Pair(T1 one, T2 two) {
		object1 = one;
		object2 = two;
	}

	public T1 getFirst() {
		return object1;
	}

	public T2 getSecond() {
		return object2;
	}
}