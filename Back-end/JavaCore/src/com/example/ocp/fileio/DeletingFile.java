package com.example.ocp.fileio;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DeletingFile {
	public static void main(String[] args) throws IOException {
		Path pathSource = Paths.get("C:\\Users\\phamt\\Desktop\\a");
		try {
			Files.delete(pathSource);
			System.out.println("File deleted successfully");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try (Stream<Path> entries = Files.list(Paths.get("."))) {
			entries.forEach(System.out::println);
		}

		Files.list(Paths.get(".")).map(path -> path.toAbsolutePath()).forEach(System.out::println);

//		Files.walk(Paths.get(".")).forEach(System.out::println);

		try (Stream<Path> entries = Files.walk(Paths.get("."), 4, FileVisitOption.FOLLOW_LINKS)) {
			long numOfEntries = entries.count();
			System.out.printf("Found %d entries in the current path", numOfEntries);

		}

		
	}
}
