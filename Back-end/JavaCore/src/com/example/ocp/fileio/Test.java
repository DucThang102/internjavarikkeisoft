package com.example.ocp.fileio;

import java.io.IOException;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Test {
	public static void main(String[] args) {
		Path testFilePath = Paths.get("D:\\test\\testfile.txt");

		System.out.println("Printing file information: ");
		System.out.println("\t file name: " + testFilePath.getFileName());
		System.out.println("\t root of the path: " + testFilePath.getRoot());
		System.out.println("\t parent of the target: " + testFilePath.getParent());

		// print path elements
		System.out.println("Printing elements of the path: ");
		System.out.println("Name count = " + testFilePath.getNameCount());
		for (Path element : testFilePath) {
			System.out.println("\t path element: " + element);
		}

		System.out.println("Uri (path can open file in browser) : " + testFilePath.toUri());
		System.out.println("AbsolutePath : " + testFilePath.toAbsolutePath());
		System.out.println("Normalize : " + testFilePath.normalize());
		try {
			System.out.println("RealPath (only file existed) : " + testFilePath.toRealPath(LinkOption.NOFOLLOW_LINKS));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.err.println("File not exist");
			e.printStackTrace();
		}

		Path dirName = Paths.get("D:\\OCPJP\\programs\\NIO2\\");
		Path resolvedPath = dirName.resolve("Test");   // nối 2 đường dẫn, trả về đường dẫn tuyệt đối
		System.out.println(resolvedPath);

	}
}
