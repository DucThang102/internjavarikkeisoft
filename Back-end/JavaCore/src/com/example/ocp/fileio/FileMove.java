package com.example.ocp.fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileMove {
	public static void main(String[] args) {
		Path pathSource = Paths.get("C:\\Users\\phamt\\Desktop\\OCPJP8_copy.pdf");
		Path pathDestination = Paths.get("C:\\Users\\phamt\\Desktop\\a\\OCPJP8_copy.pdf");
		try {
			Files.move(pathSource, pathDestination, StandardCopyOption.REPLACE_EXISTING);
			System.out.println("Source file moved !");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
