package com.example.ocp.fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileClass {
	public static void main(String[] args) throws IOException {
		Path path1 = Paths.get("OCPJP8.pdf");
		Path path2 = Paths.get("D:/Data/Java/Rikkeisoft/internjavarikkeisoft/Back-end/OCPJP8.pdf");
//		System.out.println("Files.isSameFile(path1, path2) is: " + Files.isSameFile(path1, path2));

		Path path = Paths.get("D:/Data/Java/Rikkeisoft/internjavarikkeisoft/Back-end/OCPJP8.pdf");

		if (Files.exists(path2)) {
			System.out.println("The file/directory " + path.getFileName() + " exists");
			// check whether it is a file or a directory
			if (Files.isDirectory(path)) {
				System.out.println(path.getFileName() + " is a directory");
			} else {
				System.out.println(path.getFileName() + " is a file");
			}
		} else {
			System.out.println("The file/directory " + path.getFileName() + " does not exist");
		}
	}
}
