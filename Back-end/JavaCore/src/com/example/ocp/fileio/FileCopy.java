package com.example.ocp.fileio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileCopy {
	public static void main(String[] args) {
		Path pathSource = Paths.get("D:/Data/Java/Rikkeisoft/internjavarikkeisoft/Back-end/OCPJP8.pdf");
		Path pathDestination = Paths.get("C:\\Users\\phamt\\Desktop\\OCPJP8_copy.pdf");
		try {
			if (!Files.exists(pathDestination)) {
				Files.copy(pathSource, pathDestination);
				System.out.println("Source file copied successfully !");
			}else {
				System.out.println("Destination path existed !");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
