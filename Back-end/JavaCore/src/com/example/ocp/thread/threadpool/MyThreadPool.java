package com.example.ocp.thread.threadpool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MyThreadPool {
	public static void main(String[] args) {

		
//		Cach 1
//		ArrayBlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(100);
//
//		ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5, 5, 1, TimeUnit.SECONDS, blockingQueue);
//		arg0: số tiến trình mặc định, 
//		arg1: số tiến trình tối đa có thể, 
//		arg2: thời gian thực thi
//		arg3: đơn vị thời gian
//		arg4: số tiến trình trong hàng đợi
		
//		for (int i = 0; i < 20; i++) {
//			poolExecutor.execute(new RunPool(i));
//		}
		
//		Cach 2
		ExecutorService executorService = Executors.newFixedThreadPool(5);
		for (int i = 0; i < 20; i++) {
			executorService.submit(new RunPool(i));
		}
		try {
			executorService.awaitTermination(1,TimeUnit.DAYS);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		executorService.shutdown();
	}
}

class RunPool implements Runnable {
	int id;

	public RunPool(int id) {
		this.id = id;
	}

	public void run() {
		System.out.println("Executing thread " + id + "..........");
		try {
			Thread.sleep(1000);
			System.out.println("Executed thread " + id);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}