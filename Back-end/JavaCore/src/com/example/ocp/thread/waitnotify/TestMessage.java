package com.example.ocp.thread.waitnotify;

public class TestMessage {

	public static void main(String[] args) {
		Message message = new Message();
		Thread t1 = new Thread(new Sender(message));
		Thread t2 = new Thread(new Receiver(message));
		
		t1.start();
		t2.start();
	}

}
