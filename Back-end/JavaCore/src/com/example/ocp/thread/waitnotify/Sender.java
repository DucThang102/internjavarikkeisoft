package com.example.ocp.thread.waitnotify;

public class Sender implements Runnable {

	Message message;

	public Sender(Message message) {
		this.message = message;
	}

	public void run() {
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		synchronized (message) {
			message.setContent("Hello");
			message.notifyAll();
		}

	}
}
