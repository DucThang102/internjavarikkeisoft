package com.example.ocp.thread.waitnotify;

public class Receiver implements Runnable{
	
	Message message;
	
	public Receiver(Message message) {
		this.message = message;
	}

	@Override
	public void run() {
		synchronized (message) {   // đồng bộ hóa class mesage
			System.out.println("Fetching message....................");
			try {
				message.wait();   // chờ đến khi nào bên sender notify thì mới lấy được tin nhắn
				System.out.println("Received message: "+message.getContent());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
