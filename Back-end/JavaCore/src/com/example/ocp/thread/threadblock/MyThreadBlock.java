package com.example.ocp.thread.threadblock;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyThreadBlock {

	static List<Integer> list1 = new ArrayList<Integer>();
	static List<Integer> list2 = new ArrayList<Integer>();
	Random random = new Random();

	/**
	 * synchronized: chỉ cho phép 1 tiến trình thực thi phương thức này trong 1 thời
	 * gian
	 */
	public synchronized void setList1() {
		try {
			Thread.sleep(1);
			list1.add(random.nextInt(100));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public void setList2() {
		synchronized (this) {
			try {
				Thread.sleep(1);
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		list2.add(random.nextInt(100));
	}

	public void Execute() {
		for (int i = 0; i < 1000; i++) {
			setList1();
			setList2();
		}
	}

	public static void main(String[] args) {
		MyThreadBlock myThreadBlock = new MyThreadBlock();
		System.out.println("Executing......");

		Thread thread1 = new Thread(new Runnable() {
			@Override
			public void run() {
				myThreadBlock.Execute();
			}
		});
		thread1.start();

		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				myThreadBlock.Execute();
			}
		});
		thread2.start();

		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("List 1: " + list1.size());
		System.out.println("List 2: " + list2.size());
	}
}
