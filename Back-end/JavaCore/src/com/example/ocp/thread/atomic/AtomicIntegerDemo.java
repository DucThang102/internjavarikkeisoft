package com.example.ocp.thread.atomic;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerDemo {
	static AtomicInteger ai = new AtomicInteger(10);

	public static void increment() {
		ai.incrementAndGet();
	}

	public static void decrement() {
		ai.getAndDecrement();
	}

	public static void compare() {
		ai.compareAndSet(10, 11);
	}

	public static void main(String[] args) {
		increment();
		decrement();
		compare();
		System.out.println(ai);
	}
}
