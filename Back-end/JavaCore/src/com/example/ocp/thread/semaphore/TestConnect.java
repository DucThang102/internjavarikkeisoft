package com.example.ocp.thread.semaphore;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TestConnect {

	public static void main(String[] args) {

		ExecutorService executorService = Executors.newCachedThreadPool(); // expandable thread pool

		for (int i = 0; i < 300; i++) {
			executorService.submit(new Runnable() {

				@Override
				public void run() {
					try {
						Connect.openConnect().CountConnection();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		}
		
		executorService.shutdown();
		
		try {
			executorService.awaitTermination(1, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
