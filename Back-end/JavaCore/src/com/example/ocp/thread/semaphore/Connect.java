package com.example.ocp.thread.semaphore;

import java.util.concurrent.Semaphore;

public class Connect {
	public static Connect newConnect = new Connect();
	private int count = 0;
	
	//permit 20 threads co-connect to website 
	private Semaphore semaphore = new Semaphore(20);
	
	public Connect() {
		
	}
	
	public static Connect openConnect() {
		return newConnect;
	}
	
	public void CountConnection() throws InterruptedException {
		synchronized (this) {
			semaphore.acquire(); // decreasing 1 thread 
			count++;
			System.out.println("Total Connection Present = "+count);
		}
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		synchronized (this) {
			semaphore.release();   // increasing 1 thread
			count--;
		}
	}
}
