package com.example.ocp.thread.simple;

public class MyThread extends Thread {
	public void run() {
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("In run(); thread name is: " + getName());
	}

	public static void main(String[] args) {
		MyThread myThread = new MyThread();
		myThread.run();
		System.out.println("In main(); thread name: " + Thread.currentThread().getName());
	}
}
