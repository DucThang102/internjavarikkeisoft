package com.example.ocp.thread.simple;

public class RunnableImpl implements Runnable {
	public void run() {
		System.out.println("In run(); thread name is: " + Thread.currentThread().getName());
	}

	public static void main(String[] args) {
		Thread thread = new Thread(new RunnableImpl());
		thread.start();
		System.out.println("In main(); thread name is: " + Thread.currentThread().getName());
	}
}
