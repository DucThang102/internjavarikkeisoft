package com.example.ocp.thread.deadlock;

public class Test {

	public static void main(String[] args) {
		Runner runner = new Runner();
		
		Thread thread1 = new Thread(new Runnable() {
			public void run() {
				runner.process1();
			}
		});
		
		Thread thread2 = new Thread(new Runnable() {
			public void run() {
				runner.process1();
			}
		});;
		
		thread1.start();
		thread2.start();
		
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		runner.result();
	}

}
