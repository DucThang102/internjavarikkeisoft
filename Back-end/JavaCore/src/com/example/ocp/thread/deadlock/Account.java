package com.example.ocp.thread.deadlock;

public class Account {
	int totalMoney = 10000;
	public void SendMoney(int amount) {
		totalMoney += amount;
	}
	
	public void FetchMoney(int amount) {
		totalMoney -= amount;
	}
	
	public int getTotalMoney() {
		return totalMoney;
	}
	
	public void transaction(Account sender, Account receiver, int amount ) {
		sender.SendMoney(amount);
		receiver.FetchMoney(amount);
	}
}
