package com.example.ocp.thread.deadlock;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {
	Account account1 = new Account();
	Account account2 = new Account();

	Lock lock1 = new ReentrantLock();
	Lock lock2 = new ReentrantLock();

	private void voidDeadLock(Lock lock1, Lock lock2) {
		while (true) {
			if (lock1.tryLock() && lock2.tryLock()) {
				return;
			}

			if (lock1.tryLock()) {
				lock1.unlock();
			}

			if (lock2.tryLock()) {
				lock2.unlock();
			}
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	Random random = new Random();

	public void process1() {
		for (int i = 0; i < 10000; i++) {
			voidDeadLock(lock1, lock2);
			account1.transaction(account1, account2, random.nextInt(10));
			lock1.unlock();
			lock2.unlock();
		}

	}

	public void process2() {
		for (int i = 0; i < 10000; i++) {
			voidDeadLock(lock1, lock2);
			account1.transaction(account2, account1, random.nextInt(10));
			lock1.unlock();
			lock2.unlock();
		}
	}

	public void result() {
		System.out.println("Account 1: " + account1.getTotalMoney());
		System.out.println("Account 2: " + account2.getTotalMoney());
		System.out.println("Account 1 + Account 2 = " + (account1.getTotalMoney() + account2.getTotalMoney()));
	}

}
