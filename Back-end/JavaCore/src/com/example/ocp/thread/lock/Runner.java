package com.example.ocp.thread.lock;

import java.util.Scanner;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Runner {
	int count = 0;

	Lock locker = new ReentrantLock(); // đồng bộ tiến trình tương tự synchronized

	Condition condition = locker.newCondition();

	private void increasing() {
		for (int i = 0; i < 10000; i++) {
			count++;
		}
	}

	public void Process1() {
		System.out.println("Process1 waiting .........");
		locker.lock();

		try {
			condition.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		increasing();
		locker.unlock();
	}

	public void Process2() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		locker.lock();
		increasing();

		System.out.println("Press Enter to run process 1");
		Scanner scanner = new Scanner(System.in);
		scanner.nextLine();
		scanner.close();

		condition.signalAll();
		locker.unlock();
	}

	public void ShowResult() {
		System.out.println("Result count = " + count);
	}
}
