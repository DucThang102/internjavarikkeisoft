package com.example.ocp.thread.lock;

public class Test {

	public static void main(String[] args) {
		Runner runner = new Runner();
		Thread t1 = new Thread(new Runnable() {
			public void run() {
				runner.Process1();
			}
		});
		Thread t2 = new Thread(new Runnable() {
			public void run() {
				runner.Process2();
			}
		});

		t1.start();
		t2.start();

		try {
			t1.join();
			t2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		runner.ShowResult();

	}

}
