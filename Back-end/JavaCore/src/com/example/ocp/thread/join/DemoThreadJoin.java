package com.example.ocp.thread.join;

public class DemoThreadJoin {
	int count = 0;

	public static void main(String[] args) {
		DemoThreadJoin demoThreadJoin = new DemoThreadJoin();
		demoThreadJoin.RunThread();
	}

	public void RunThread() {
		Thread thread1 = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 500; i++) {
					count++;
				}
			}
		});

		Thread thread2 = new Thread(new Runnable() {

			@Override
			public void run() {
				for (int i = 0; i < 500; i++) {
					count++;
				}
			}
		});

		thread1.start();
		thread2.start();

		try {
			thread1.join();  // chờ tiến trình 1 kết thúc thì mới chạy tiến trình 2
			thread2.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		System.out.println("Count = " + count);
	}
}
