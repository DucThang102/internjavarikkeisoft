package com.example.ocp.jdbc.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBCConnection {

	public static Connection getJDBCConnecion() {
		final String url = "jdbc:mysql://localhost:3306/demo?autoReconnect=true&useSSL=false";
//		final String url = "jdbc:sqlserver://localhost:1433;databaseName=BanThuocThuY";
		final String user = "root";
		final String password = "root";
		try {
			Class.forName("com.mysql.jdbc.Driver");
//			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			return DriverManager.getConnection(url, user, password);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static void closeConnection(Connection conn) {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException ex) {
				Logger.getLogger(JDBCConnection.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}
}
