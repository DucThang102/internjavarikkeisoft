package com.example.ocp.jdbc.dao;

import java.lang.Thread.State;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.naming.spi.DirStateFactory.Result;

import com.example.ocp.jdbc.entity.User;
import com.example.ocp.jdbc.utils.JDBCConnection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class UserDAO {

	public ArrayList<User> getListUser() {
		ArrayList<User> list = new ArrayList<User>();
		try {
			Connection connection = JDBCConnection.getJDBCConnecion();
			Statement statement = (Statement) connection.createStatement();

			String sql = "Select * from user";
			ResultSet resultSet = statement.executeQuery(sql);
			while (resultSet.next()) {
				int id = resultSet.getInt("id");
				String name = resultSet.getString("name");
				int phone = resultSet.getInt("phone");
				User user = new User(id, name, phone);
				list.add(user);
			}
			JDBCConnection.closeConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}

	public boolean insertUser(User user) {
		Connection connection = JDBCConnection.getJDBCConnecion();

		try {
			Statement statement = (Statement) connection.createStatement();
			String sql = "insert into user (name,phone) values" + " ('" + user.getName() + "'," + user.getPhone() + ")";
			int rs = statement.executeUpdate(sql);
			if (rs > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateUser(User user) {
		Connection connection = JDBCConnection.getJDBCConnecion();

		try {
			Statement statement = (Statement) connection.createStatement();
			String sql = "update user set name = '" + user.getName() + "', phone = " + user.getPhone() + " where id = "
					+ user.getId();
			System.out.println(sql);
			int rs = statement.executeUpdate(sql);
			if (rs > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteUser(User user) {
		Connection connection = JDBCConnection.getJDBCConnecion();

		try {
			Statement statement = (Statement) connection.createStatement();
			String sql = "delete from user where id = " + user.getId();
			System.out.println(sql);
			int rs = statement.executeUpdate(sql);
			if (rs > 0) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	// prepare statement

	public User getUserById(int id) {
		Connection connection = JDBCConnection.getJDBCConnecion();
		String sql = "select * from user where id = ?";

		try {
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareCall(sql);

			preparedStatement.setInt(1, id);
			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				User user = new User(id, rs.getString("name"), rs.getInt("phone"));
				return user;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean insertUser2(User user) {
		Connection connection = JDBCConnection.getJDBCConnecion();
		String sql = "insert into user (name,phone) values (?, ?)";

		try {
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareCall(sql);

			preparedStatement.setString(1, user.getName());
			preparedStatement.setInt(2, user.getPhone());

			int rs = preparedStatement.executeUpdate();
			if (rs > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean updateUser2(User user) {
		Connection connection = JDBCConnection.getJDBCConnecion();
		String sql = "update user set name = ?, phone = ? where id = ?";
		try {
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareCall(sql);

			preparedStatement.setString(1, user.getName());
			preparedStatement.setInt(2, user.getPhone());
			preparedStatement.setInt(3, user.getId());

			int rs = preparedStatement.executeUpdate();
			if (rs > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean deleteUser2(User user) {
		Connection connection = JDBCConnection.getJDBCConnecion();

		String sql = "delete from user where id = ?";
		try {
			PreparedStatement preparedStatement = (PreparedStatement) connection.prepareCall(sql);

			preparedStatement.setInt(1, user.getId());

			int rs = preparedStatement.executeUpdate();
			if (rs > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
