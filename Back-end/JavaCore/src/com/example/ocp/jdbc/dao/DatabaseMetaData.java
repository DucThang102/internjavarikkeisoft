package com.example.ocp.jdbc.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.example.ocp.jdbc.utils.JDBCConnection;

public class DatabaseMetaData {
	public static void main(String[] args) throws SQLException {
		Connection connection = JDBCConnection.getJDBCConnecion();
		
		java.sql.DatabaseMetaData data = (java.sql.DatabaseMetaData) connection.getMetaData();
		System.out.println(data.getUserName());
		System.out.println(data.getDatabaseProductName());
		System.out.println(data.getURL());
	}
}
