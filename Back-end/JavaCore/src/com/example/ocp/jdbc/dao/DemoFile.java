package com.example.ocp.jdbc.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.ocp.jdbc.utils.JDBCConnection;

public class DemoFile {
	public static void main(String[] args) throws SQLException, IOException {
		
//		saveFile();
		readFile();
		
	}
	
	private static void readFile() throws SQLException, IOException {
		Connection connection  = JDBCConnection.getJDBCConnecion();
		String sql = "select * from file";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next()) {
			String name = resultSet.getString("name");
			Blob file = resultSet.getBlob("file");
			byte[] b = file.getBytes(1, (int) file.length());
			
			FileOutputStream fileOutputStream = new FileOutputStream("read_"+name);
			fileOutputStream.write(b);
			fileOutputStream.close();
			
		}
	}

	public static void saveFile() throws FileNotFoundException, SQLException  {
		File file = new File("test.txt");
		FileInputStream fileInputStream = new FileInputStream(file);
		
		Connection connection  = JDBCConnection.getJDBCConnecion();
		String sql = "insert into file(name,file) values (?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setString(1,"test.txt");
		preparedStatement.setBinaryStream(2, fileInputStream);
		
		if (preparedStatement.executeUpdate() > 0) {
			System.out.println("Successful !");
		}else {
			System.out.println("Fail !");
		}
	}
}
