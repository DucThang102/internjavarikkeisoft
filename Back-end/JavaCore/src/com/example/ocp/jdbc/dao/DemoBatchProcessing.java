package com.example.ocp.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.example.ocp.jdbc.utils.JDBCConnection;
import com.mysql.jdbc.Statement;

public class DemoBatchProcessing {
	public static void main(String[] args) throws SQLException {
		UserDAO userDAO = new UserDAO();
		Connection connection = JDBCConnection.getJDBCConnecion();
		String sql1 = "insert into user (name, phone) values ('A', 098765543)";
		String sql2 = "insert into user (name, phone) values ('B', 098676522)";
		
		Statement statement = (Statement) connection.createStatement();
		statement.addBatch(sql1);
		statement.addBatch(sql2);

		statement.executeBatch();
		
		String sql = "insert into user(name,phone) values (?,?)";
		PreparedStatement preparedStatement = connection.prepareStatement(sql);
		
		preparedStatement.setString(1,"C");
		preparedStatement.setInt(2,989675);
		preparedStatement.addBatch();
		
		preparedStatement.setString(1,"D");
		preparedStatement.setInt(2,3452678);
		preparedStatement.addBatch();
		
		preparedStatement.executeBatch();
		
		System.out.println("List User: " + userDAO.getListUser().toString());
		
	}
}
