package com.example.ocp.jdbc.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.ocp.jdbc.entity.User;
import com.example.ocp.jdbc.utils.JDBCConnection;

public class CallProcedure {
	public static void main(String[] args) throws SQLException {
		System.out.println(getUserById(1).toString());
	}

	public static User getUserById(int id) throws SQLException {
		User user = new User();
		Connection connection = JDBCConnection.getJDBCConnecion();
		CallableStatement callableStatement = (CallableStatement) connection
				.prepareCall("{call getUserByID(?)}");
		callableStatement.setInt(1, 1);

		ResultSet resultSet = callableStatement.executeQuery();
		while (resultSet.next()) {
			user.setId(id);
			user.setName(resultSet.getString("name"));
			user.setPhone(resultSet.getInt("phone"));
		}
		return user;
	}
}
