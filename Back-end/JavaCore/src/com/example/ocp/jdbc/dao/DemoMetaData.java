package com.example.ocp.jdbc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import com.example.ocp.jdbc.utils.JDBCConnection;
import com.mysql.jdbc.Statement;

public class DemoMetaData {
	public static void main(String[] args) throws SQLException {
		Connection connection = JDBCConnection.getJDBCConnecion();
		Statement statement = (Statement) connection.createStatement();
		
		String sql = "select * from user";
		
		ResultSet resultSet = statement.executeQuery(sql);
		
		ResultSetMetaData  metaData = resultSet.getMetaData();
		
		System.out.println(metaData.getColumnCount());
		System.out.println(metaData.getColumnName(1));
		System.out.println(metaData.getColumnTypeName(1));
		System.out.println(metaData.getTableName(1));
		
	}
}
