package com.example.ocp.jdbc.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.example.ocp.jdbc.utils.JDBCConnection;

public class DemoTransaction {
	public static void main(String[] args) throws SQLException {
		Connection connection = JDBCConnection.getJDBCConnecion();
		Statement statement = connection.createStatement();
		
		connection.setAutoCommit(false);
		
		String sql1 = "delete from user where id = 12";
		String sql2 = "insert into user(id, name, phone) values (1, 'Nam', 08676564)";
		
		statement.executeUpdate(sql1);
		statement.executeUpdate(sql2);
		
		connection.commit();
	}
}
