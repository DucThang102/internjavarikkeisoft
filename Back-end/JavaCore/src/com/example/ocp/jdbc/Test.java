package com.example.ocp.jdbc;

import java.util.Scanner;

import com.example.ocp.jdbc.dao.UserDAO;
import com.example.ocp.jdbc.entity.User;

public class Test {
	public static void main(String[] args) {
		UserDAO userDAO = new UserDAO();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Add new user: ");
		System.out.print("Name = ");
		User user = new User();
		user.setName(scanner.nextLine());
		System.out.print("Phone = ");
		user.setPhone(scanner.nextInt());

		if (userDAO.insertUser2(user)) {
			System.out.println("Insert successful");
			System.out.println("List User: " + userDAO.getListUser().toString());
		} else {
			System.out.println("Insert fail");
		}

		System.out.println("Update user");
		System.out.print("type user id: ");

		User userUpdate = new User();
		userUpdate.setId(scanner.nextInt());
		scanner.nextLine();
		System.out.print("Name = ");
		userUpdate.setName(scanner.nextLine());
		System.out.print("Phone = ");
		userUpdate.setPhone(scanner.nextInt());

		if (userDAO.updateUser2(userUpdate)) {
			System.out.println("update successful");
			System.out.println("List User: " + userDAO.getListUser().toString());
		} else {
			System.out.println("update fail");
		}

		System.out.print("Delete user having id ");
		User deleteUser = new User();
		deleteUser.setId(scanner.nextInt());
		if (userDAO.deleteUser2(deleteUser)) {
			System.out.println("deleted");
			System.out.println("List User: " + userDAO.getListUser().toString());
		} else {
			System.out.println("delete fail");
		}
		
		System.out.println("List User: " + userDAO.getListUser().toString());
//		System.out.println(userDAO.getUserById(1).toString());

	}
}
