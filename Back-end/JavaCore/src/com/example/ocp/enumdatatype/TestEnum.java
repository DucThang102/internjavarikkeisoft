package com.example.ocp.enumdatatype;

public class TestEnum {
	PrinterType printerType;
	
	public TestEnum(PrinterType pType) {
		printerType = pType;
	}

	public void feature() {
		// switch based on the printer type passed in the constructor
		switch (printerType) {
		case DOTMATRIX:
			System.out.println("Dot-matrix printers are economical and almost obsolete");
			break;
		case INKJET:
			System.out.println("Inkjet printers provide decent quality prints");
			break;
		case LASER:
			System.out.println("Laser printers provide best quality prints");
			break;
		}
		
		System.out.println("Print page capacity per minute: " +
				 printerType.getPrintPageCapacity());
	}

	public static void main(String[] args) {
		TestEnum testEnum = new TestEnum(PrinterType.LASER);
		testEnum.feature();
	}
}

//define an enum for classifying printer types	
enum PrinterType {
	DOTMATRIX(5), INKJET(10), LASER(50);

	private int pagePrintCapacity;

	private PrinterType(int pagePrintCapacity) {
		this.pagePrintCapacity = pagePrintCapacity;
	}

	public int getPrintPageCapacity() {
		return pagePrintCapacity;
	}
}