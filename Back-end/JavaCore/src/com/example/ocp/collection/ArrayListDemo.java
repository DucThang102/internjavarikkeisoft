package com.example.ocp.collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ArrayListDemo {
	public static void main(String[] args) {
		ArrayList<String> languageList = new ArrayList<>();
		languageList.add("C");
		languageList.add("C++");
		languageList.add("Java");

		for (String language : languageList) {
			System.out.println(language);
		}

		for (Iterator<String> languageIter = languageList.iterator(); languageIter.hasNext();) {
			String language = languageIter.next();
			System.out.println(language);
		}

		ArrayList<Integer> nums = new ArrayList<Integer>();
		for (int i = 1; i < 10; i++)
			nums.add(i);
		System.out.println("Original list " + nums);
		Iterator<Integer> numsIter = nums.iterator();
		while (numsIter.hasNext()) {
			numsIter.next();
			numsIter.remove();
		}

		System.out.println("List after removing all elements" + nums);
		
		ArrayAsList();
	}

	public static void ArrayAsList() {
		Double[] temperatureArray = { 31.1, 30.0, 32.5, 34.9, 33.7, 27.8 };
		System.out.println("The original array is: " + Arrays.toString(temperatureArray));
		List<Double> temperatureList = Arrays.asList(temperatureArray);
		temperatureList.set(0, 35.2);
		System.out.println("The modified array is: " + Arrays.toString(temperatureArray));
	}
}
