package com.example.ocp.collection;

import java.util.Set;
import java.util.TreeSet;

/**
 * lưu các phần tử có sắp xếp theo thứ tự
 * 
 */
public class TreeSetDemo {

	public static void main(String[] args) {
		String pangram = "the quick brown fox jumps over the lazy dog";
		Set<Character> aToZee = new TreeSet<Character>();
		for (char gram : pangram.toCharArray()) {
			aToZee.add(gram);
		}
		System.out.println("The pangram is: " + pangram);
		System.out.print("Sorted pangram characters are: " + aToZee);
	}

}
