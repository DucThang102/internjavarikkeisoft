package com.example.ocp.collection;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * A Map stores key and value pairs. Key is unique not null, value can be null
 */
public class MapDemo {
	public static void main(String[] args) {
//		HashMapExample();
//		TreeMapExample();
		NavigableMapDemo();
	}

	/**
	 * HashMap: tìm kiếm nhanh, không lưu theo bất kỳ thứ tự nào
	 */
	public static void HashMapExample() {
		HashMap<String, String> capitalCities = new HashMap<String, String>();
		// Add keys and values (Country, City)
		capitalCities.put("England", "London");
		capitalCities.put("Germany", "Berlin");
		capitalCities.put("Norway", "Oslo");
		capitalCities.put("USA", "Washington DC");
		if (capitalCities.remove("USA") != null) {
			System.out.println("removed USA");
		}
		if (capitalCities.containsKey("England")) {
			System.out.println("contain key England");
		}
		if (capitalCities.containsValue("Berlin")) {
			System.out.println("contain value Beerlin");
		}
		System.out.println(capitalCities);
	}

	/**
	 * TreeMap: lưu theo thứ tự, tìm kiếm, thêm phần tử chậm hơn hashMap
	 */
	public static void TreeMapExample() {
		Map<Integer, String> map = new TreeMap<Integer, String>();
		map.put(2, "OOP");
		map.put(1, "Basic java");
		map.put(4, "Multi-Thread");
		map.put(3, "Collection");
		map.put(7, "java");
		map.put(5, null);

		System.out.println(map.toString());

		map.remove(7);

		System.out.println("show map using method keySet()");
		for (Integer key : map.keySet()) {
			String value = map.get(key);
			System.out.println(key + " : " + value);
		}

		System.out.println("show map using method EntrySet()");
		for (Entry<Integer, String> entry : map.entrySet()) {
			System.out.println(entry.getKey() + " : " + entry.getValue());
		}

		map.clear();
		System.out.println("clear map");
		System.out.println(map.toString());
	}

	/**
	 * NavigableMap: lưu theo thứ tự key tăng dần
	 * */
	public static void NavigableMapDemo() {
		NavigableMap<Integer, String> examScores = new TreeMap<Integer, String>();
		examScores.put(90, "Sophia");
		examScores.put(20, "Isabella");
		examScores.put(10, "Emma");
		examScores.put(50, "Olivea");
		
		System.out.println("The data in the map is: " + examScores);
		System.out.println("The data descending order is: " + examScores.descendingMap());
		System.out.println("Details of those who passed the exam: " + examScores.tailMap(40));
		System.out.println("The lowest mark is: " + examScores.firstEntry());
		System.out.println("The highest mark is: " + examScores.lastEntry().getValue());
	}
}
