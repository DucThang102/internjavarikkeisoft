package com.example.ocp.collection;

import java.util.ArrayDeque;
import java.util.Deque;

public class DequeDemo {
	public static void main(String[] args) {
		Deque<String> deque = new ArrayDeque<String>();
		deque.add("Thang");
		deque.add("Dung");
		deque.addLast("Tung");
		deque.add("Minh");
		deque.addFirst("Tu");
		deque.push("Nam");  // thêm vào đầu tiên
		
		System.out.println(deque.toString());
		System.out.println(deque.peekFirst());
		System.out.println(deque.peek());  // xem phần tử đầu tiên 
		System.out.println(deque.peekLast());
		System.out.println(deque.pop());   // lấy ra phần tử đầu tiên
		System.out.println(deque.poll());   // lấy ra phần tử đầu tiên
		System.out.println(deque.toString());
		
	}
}
