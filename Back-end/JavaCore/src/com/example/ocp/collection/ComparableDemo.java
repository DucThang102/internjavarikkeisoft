package com.example.ocp.collection;

import java.util.Arrays;
import java.util.Comparator;

public class ComparableDemo {

	public static void main(String[] args) {
		Student[] students = { new Student("as011", "Lennon ", 3.1), new Student("vs021", "McCartney", 3.4),
				new Student("ds012", "Harrison ", 2.7), new Student("bs022", "Starr ", 3.7) };

		System.out.println("Before sorting by student CGPA");
		System.out.println("Student-ID \t Name \t CGPA (for 4.0) ");
		System.out.println(Arrays.toString(students));

		Arrays.sort(students);

		System.out.println("After sorting by student CGPA");
		System.out.println("Student-ID \t Name \t CGPA (for 4.0) ");
		System.out.println(Arrays.toString(students));

		Arrays.sort(students, new Comparator<Student>() {
			@Override
			public int compare(Student o1, Student o2) {
				return o1.name.compareTo(o2.name);
			}
		});

		System.out.println("After sorting by student name");
		System.out.println("Student-ID \t Name \t CGPA (for 4.0) ");
		System.out.println(Arrays.toString(students));
	}

}

class Student implements Comparable<Student> {
	String id;
	String name;
	Double cgpa;

	public Student(String studentId, String studentName, double studentCGPA) {
		id = studentId;
		name = studentName;
		cgpa = studentCGPA;
	}

	public String toString() {
		return " \n " + id + " \t " + name + " \t " + cgpa;
	}

	public int compareTo(Student that) {
		return this.cgpa.compareTo(that.cgpa);
	}
}
