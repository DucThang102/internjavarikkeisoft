package com.example.ocp.collection;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class ExamTest {

	private static boolean removeVowels(int c) {
		switch (c) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		List<Integer> intList = new LinkedList<>();
		List<Double> dblList = new LinkedList<>();
		System.out.println("First type: " + intList.getClass());
		System.out.println("Second type:" + dblList.getClass());

		String[] brics = { "Brazil", "Russia", "India", "China" };
		Arrays.sort(brics, null); // LINE A
		for (String country : brics) {
			System.out.print(country + " ");
		}

		"abracadabra".chars().distinct().forEach(ch -> System.out.printf("%c ", ch));

		"avada kedavra".chars().filter(ExamTest::removeVowels).forEach(ch -> System.out.printf("%c", ch)); // aaaeaa

		Deque<Integer> deque = new ArrayDeque<>();
		deque.addAll(Arrays.asList(1, 2, 3, 4, 5));
		System.out.println("The removed element is: " + deque.remove());
	}

}
