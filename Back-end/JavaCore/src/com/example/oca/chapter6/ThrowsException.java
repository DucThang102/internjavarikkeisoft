package com.example.oca.chapter6;

public class ThrowsException extends Exception {

	public void go() {
		System.out.print("A");
		try {
			stop();
		} catch (ArithmeticException e) {
			System.out.print("B");
		} finally {
			System.out.print("C");
		}
		System.out.print("D");
	}

	public void stop() {
		System.out.print("E");
		Object x = null;
		x.toString();
		System.out.print("F");
	}
	
	static int devide() {
		int a =0, b=0;
		try {
			return a / b;
		} catch (RuntimeException e) {
			return -1;
//		} catch (ArithmeticException e) {  // Unreachable catch block for ArithmeticException. It is already handled by the catch block for RuntimeException
//			return 0;
		} finally {
			System.out.print("done");
		}
	}

	public static void main(String[] args) {
		 new ThrowsException().go(); // print: AE
	}

	// cach 1: // handle exception
//	public static void main(String[] args) {
//		try {
//			eatCarrot();
//		} catch (ThrowsException e) {
//			// TODO: handle exception
//		}
//	}

	// cach 2: // declare exception
//	public static void main(String[] args) throws ThrowsException {
//		eatCarrot();
//	}

	private static void eatCarrot() throws ThrowsException {
	}
}

class DoSomething {
	public void go() {
		System.out.print("A");
		try {
			stop();
		} catch (ArithmeticException e) {
			System.out.print("B");
		} finally {
			System.out.print("C");
		}
		System.out.print("D");
	}

	public void stop() {
		System.out.print("E");
		Object x = null;
		x.toString();
		System.out.print("F");
	}

	public static void main(String[] args) {
		new DoSomething().go();
	}
}
