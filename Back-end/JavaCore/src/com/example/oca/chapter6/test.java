package com.example.oca.chapter6;

public class test {
	static String name;
	public static void main(String[] args) {
		explore();
	}

	static void explore() {
		try {

//			// ArithmeticException / by zero
//			int answer = 11 / 0;
//
//			// ArrayIndexOutOfBoundsException
//			int[] countsOfMoose = new int[3];
//			System.out.println(countsOfMoose[-1]);
//
//			// ClassCastException
//			String type = "moose";
//			Integer number = (Integer) type; // DOES NOT COMPILE
			
			//NullPointerException
//			System.out.println(name.length());
			
			// NumberFormatException
			Integer.parseInt("abc");

//		
		} catch (RuntimeException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} finally {
			System.out.println("always get finally");
		}

	}


}
