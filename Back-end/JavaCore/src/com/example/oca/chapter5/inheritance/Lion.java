package com.example.oca.chapter5.inheritance;

public class Lion extends Animal {

	public Lion() {
		super();
	}

	public Lion(int age) {
		super(age);
	}

	private void roar() {
		System.out.println("The " + getAge() + " year old lion says: Roar!");
	}

}
