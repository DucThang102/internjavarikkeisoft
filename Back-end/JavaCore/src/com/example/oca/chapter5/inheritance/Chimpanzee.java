package com.example.oca.chapter5.inheritance;

class Primate {
	public Primate() {
		System.out.println("Primate");
	}
}

class Ape extends Primate {
	public Ape() {
		System.out.println("Ape");
	}
}

public class Chimpanzee extends Ape {
	public static void main(String[] args) {
		new Chimpanzee();        // auto create default constructor call super() constructor of parent class
		//Primate
		//Ape
	}
}
