package com.example.oca.chapter5.inheritance;

public class Eagle extends Bird {

	public int fly(int height) { // overloading: having 1 parameter
		System.out.println("Bird is flying at " + height + " meters");
		return height;
	}

//	public int eat(int food) { // DOES NOT COMPILE: return type is incompatible with parent method
//		System.out.println("Bird is eating " + food + " units of food");
//		return food;
//	}

	public static void hiddingMethod() {
		System.out.println("method of sub class");
	}

//	public final boolean hasFeathers() { // DOES NOT COMPILE
//		return false;
//	}

	public static void main(String[] args) {
		Eagle.hiddingMethod();
	}
}
