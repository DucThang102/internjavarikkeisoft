package com.example.oca.chapter5.inheritance;

public class Bird {

	public void fly() {
		System.out.println("Bird is flying");
	}

	public void eat(int food) {
		System.out.println("Bird is eating " + food + " units of food");
	}

	public static void hiddingMethod() {
		System.out.println("method of super class");
	}

	public final boolean hasFeathers() {
		return true;
	}
}
