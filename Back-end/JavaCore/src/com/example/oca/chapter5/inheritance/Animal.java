package com.example.oca.chapter5.inheritance;

public class Animal {
	private int age;

	
	public Animal() {
		super();
	}

	public Animal(int age) {
		super();
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

}
