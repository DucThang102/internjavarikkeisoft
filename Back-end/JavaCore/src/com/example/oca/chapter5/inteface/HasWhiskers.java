package com.example.oca.chapter5.inteface;

public interface HasWhiskers {
	public int getNumberOfWhiskers();
}
