package com.example.oca.chapter5.inteface;

public interface HasTail {
	public int getTailLength();
}
