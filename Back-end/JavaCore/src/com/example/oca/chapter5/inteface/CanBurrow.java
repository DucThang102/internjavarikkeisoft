package com.example.oca.chapter5.inteface;

/**
 * 1. Không thể khởi tạo trực tiếp 
 * 2. Không yêu cầu phải có bất kỳ phương thức nào 
 * 3. Có thể không được đánh dấu là final 
 * 4. Tất cả các interface ở mức cao nhất thì được coi là có quyền truy cập public hoặc default 
 * 5. Tất cả các phương thức trong interface đều được coi là abstract và public
 */
public interface CanBurrow {
	// luôn là public static final cho dù có để default
	public static final int MINIMUM_DEPTH = 2;

	public abstract int getMaximumDepth();

//	private void dig(int depth); // DOES NOT COMPILE
//
//	protected abstract double depth(); // DOES NOT COMPILE
//
//	public final void surface(); // DOES NOT COMPILE

	/**
	 * 1. chỉ đc định nghĩa trong interface 
	 * 2. phải đc định ngĩa với "default" và phải khai báo thân phương thức 
	 * 3. không được coi là static, final, abstract vì có thể được sử dụng 
	 *    hoặc ghi đè bởi class thực thi interface này 
	 * 4. giống với tất cả các phương thức trong interface, luôn được coi là public và không
	 *    biên dịch được nếu để là private/protected
	 */
	public default double getTemperature() {
		return 10.0;
	}

	/**
	 * 1. giống với tất cả các phương thức trong interface, luôn được coi là public và không
	 *    biên dịch được nếu để là private/protected
	 * 2. để tham chiếu tới 1 static method, thông qua interfaceName.staticMethodName
	 * */
	static int getJumpHeight() {
		return 8;
	}
}
