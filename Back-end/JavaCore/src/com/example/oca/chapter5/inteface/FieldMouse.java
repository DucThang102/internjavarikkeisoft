package com.example.oca.chapter5.inteface;

public class FieldMouse implements CanBurrow, HasTail, HasWhiskers {

	@Override
	public int getMaximumDepth() {
		//call static method in parent interface
		CanBurrow.getJumpHeight();
		return 0;
	}

	@Override
	public int getNumberOfWhiskers() {
		return 0;
	}

	@Override
	public int getTailLength() {
		return 0;
	}

	

}
