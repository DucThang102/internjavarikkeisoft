package com.example.oca.chapter5.polymorphism;

public class Capybara extends Rodent {
	public static void main(String[] args) {
		Rodent rodent = new Rodent();

		Capybara capybara = (Capybara) rodent; // rule 4: Throws ClassCastException at runtime because rodent is not a instance
		// of Capybara
		if (rodent instanceof Capybara) {
			Capybara capybara1 = (Capybara) rodent;
		}
	}
}

class Rodent {

}