package com.example.oca.chapter5.polymorphism;

public class PolymophismParameter {

	/**
	 * parameter is parent class Reptile
	 * can pass object is instance of subclass
	 * */
	public static void feed(Reptile reptile) {
		System.out.println("Feeding reptile " + reptile.getName());
	}

	public static void main(String[] args) {
		feed(new Alligator());
		feed(new Crocodile());
		feed(new Reptile());
	}
}

class Reptile {
	public String getName() {
		return "Reptile";
	}
}

class Alligator extends Reptile {
	public String getName() {
		return "Alligator";
	}
}

class Crocodile extends Reptile {
	public String getName() {
		return "Crocodile";
	}
}
