package com.example.oca.chapter5.polymorphism;

public class Lemur  extends Primate implements HasTail {

	@Override
	public boolean isTailStriped() {
		return false;
	}

	 public int age = 10;
	 
	 public static void main(String[] args) {
		Lemur lemur = new Lemur();
		System.out.println(lemur.age);
		
		HasTail hasTail = lemur;
		System.out.println(hasTail.isTailStriped());
//		System.out.println(hasTail.age); // DOES NOT COMPILE
		
		Primate primate = lemur;  //rule 1s
		System.out.println(primate.hasHair());
//		 System.out.println(primate.isTailStriped()); // DOES NOT COMPILE
		
		Lemur lemur2 = (Lemur) primate;  // rule 2
		/*
		 * Casting object rule
		 * 1. Ép kiểu 1 đối tượng từ class con sang class cha không yêu cầu ép kiểu rõ ràng
		 * 2. Ép kiểu 1 đối tượng từ class cha sang class con yêu cầu ép kiểu rõ ràng 
		 * 3. Trình biên dịch sẽ không cho phép ép sang kiểu không liên quan
		 * 4. Mặc dù khi code biên dịch mà không có vấn đề gì, một ngọai có thể được ném ra trong 
		 * 		thời gian chạy nếu đối tượng đang được ép kiểu không thực sự là một thể hiện của lớp đó 
		 * */
	}
}
