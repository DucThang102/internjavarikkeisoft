package com.example.oca.chapter5.polymorphism;

public interface HasTail {
	 public boolean isTailStriped();
}
