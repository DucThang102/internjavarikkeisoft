package com.example.oca.chapter5.abstractclass;

/**
 * Abstract Class Definition Rules: 1. không thể khởi tạo đối tượng từ abstract
 * class 2. có thể định nghĩa bao nhiêu abstract method và non-abstract method
 * cũng được 3. không thể khai báo abstract class là private hay final 4. một
 * abstract class kế thừa 1 abstract class khác kế thừa tất cả abstract method
 * của cha như là các abstract method của chính nó 5. class không phải abstract
 * đầu tiên kế thừa abstract class thì phải viết thân phương thức cho tất cả các
 * abstract method được kế thừa
 */
public abstract class Animal {

	protected int age;

	public void eat() {
		System.out.println("Animal is eating");
	}

	/**
	 * Abstract Method Definition Rules: 1. chỉ được định nghĩa trong abstract class
	 * 2. không được khai báo private hay final 3. không được khai báo thân phương
	 * thức 4. thực thi abstract method trong subclass theo đúng quy tắc của
	 * overidding method: cùng tên, cùng kiểu dữ liệu trả về, access modifier ít
	 * nhất phải bằng hoặc rộng hơn ở phương thức cha.
	 * 
	 */
	public abstract String getName();

}
