package com.example.oca.chapter5.review;

public interface Rodent {
	default boolean isBlind() {
		return true;
	}
}

interface HasVocalCords {
	public abstract void makeSound();
}

interface CanBark extends HasVocalCords {
	public void bark();
}

interface Aquatic {
	public default int getNumberOfGills(int input) {
		return 2;
	}
}