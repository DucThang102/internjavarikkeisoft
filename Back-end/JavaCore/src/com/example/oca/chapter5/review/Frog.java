package com.example.oca.chapter5.review;

public class Frog implements CanHop {
	public static void main(String[] args) {
		Object frog = new TurtleFrog();
	}
}

class BrazilianHornedFrog extends Frog {
}

class TurtleFrog extends Frog {
}
