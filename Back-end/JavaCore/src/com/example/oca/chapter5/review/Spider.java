package com.example.oca.chapter5.review;

//public class Spider extends Arthropod implements CanBark, Aquatic {
//	public void printName(int input) {
//		System.out.print("Spider");
//	}
//
//	public String getNumberOfGills() {
//		return "4";
//	}
//
////	public String getNumberOfGills(int input) {
////		return "6";
////	}
//
////	public static void main(String[] args) {
////		System.out.println(new Spider().getNumberOfGills(-1));
////	}
//
////	public static void main(String[] args) {
////		Spider spider = new Spider();
////		spider.printName(4);
////		spider.printName(9.0);
////	}
//
//	@Override
//	public void makeSound() {
//		// TODO Auto-generated method stub
//
//	}
//
//	@Override
//	public void bark() {
//		// TODO Auto-generated method stub
//
//	}
//}

class Arthropod {
	public void printName(double input) {
		System.out.print("Arthropod");
	}
}

abstract class Reptile {
	public final void layEggs() {
		System.out.println("Reptile laying eggs");
	}

	public static void main(String[] args) {
		Reptile reptile = new Lizard();
		reptile.layEggs();
	}
}

class Lizard extends Reptile {
//	public void layEggs() {
//		System.out.println("Lizard laying eggs");
//	}
}

 public abstract class Spider {
	private void fly() {
		System.out.println("Bird is flying");
	}

	public static void main(String[] args) {
		Spider bird = new Pelican();
		bird.fly();
	}
}

class Pelican extends Spider {
	protected void fly() {
		System.out.println("Pelican is flying");
	}
}