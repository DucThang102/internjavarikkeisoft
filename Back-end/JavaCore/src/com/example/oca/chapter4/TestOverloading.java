package com.example.oca.chapter4;

public class TestOverloading {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Person p1 = new Person("Thang", 21);
		Person p2 = new Person("Minh", 20);

		swap(p1, p2);
		System.out.println("in global: " + p1.toString());
		System.out.println("in global: " + p2.toString());

		int a = 10, b = 20;
		swap(a, b);
		System.out.println("in global: " + a);
		System.out.println("in global: " + b);

		TestOverloading o = new TestOverloading();
		o.fly("test");
		o.fly(56); // auto boxing int to Integer

//		play(4); // DOES NOT COMPILE: cannot handle converting in 2 step: int -> long, long -> Long 	 	 	
		play(4L); // calls the Long version

	}

	public static void swap(Person p1, Person p2) {
		Person temp = p1;
		p1 = p2;
		p2 = temp;
		System.out.println("in local: " + p1.toString());
		System.out.println("in local: " + p2.toString());
	}

	public static void swap(int a, int b) {
		int temp = a;
		a = b;
		b = temp;
		System.out.println("in local: " + a);
		System.out.println("in local: " + b);
	}

	public void fly(int numMiles, short shortValue) {
	}

	public void fly(short shortValue, int numMiles) {
	}

	// do not compile
//	public int fly(short shortValue, int numMiles) {
//	}
//	public static int fly(short shortValue, int numMiles) {
//	}

	public void fly(String s) {
		System.out.print("string ");
	}

	public void fly(Object o) {
		System.out.print("object ");
	}

	public static void play(Long l) {
	}

	public static void play(Long... l) {
	}

}
