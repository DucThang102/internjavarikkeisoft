package com.example.oca.chapter4;

public class Person { 
//	đóng gói:  chỉ sử dụng được các thuộc tính trong class qua các phương thức 
//	data(thuộc tính): private
//	getter/setter: public
	private String name;
	private int age;
	private final boolean gender;

	public Person() {
		gender = false;
	}

	public Person(String name, int age, boolean gender) {
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	
	public Person(String name, int age) {
		this.name = name;
		this.age = age;
		this.gender = false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + "]";
	}

}
