package com.example.oca.chapter4;

@FunctionalInterface
interface Hello {
	public String sayHello(String name);
}

@FunctionalInterface
interface Hallo {
	public String sayHello(String name, String com);
}
