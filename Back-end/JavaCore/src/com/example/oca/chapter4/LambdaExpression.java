package com.example.oca.chapter4;

public class LambdaExpression {

	public static void main(String[] args) {
		Hello s = name -> "Hello " + name;
		System.out.println(s.sayHello("Lambda"));

		Hallo h = (name, com) -> "Hello " + name + ". Welcome to " + com;
		System.out.println(h.sayHello("newbie", "Lambda"));
	}

}
