package com.ducthang.webservice.service;

import com.ducthang.webservice.entities.User;

public interface UserService {
	public User findUserByEmail(String email);

	public User findUserByResetToken(String resetToken);

	public void saveUser(User user);
}
