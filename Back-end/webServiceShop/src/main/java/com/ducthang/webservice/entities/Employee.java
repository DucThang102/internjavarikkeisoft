package com.ducthang.webservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "employee")
public class Employee {
	private Long empId;
	private String empNo;
	private String empName;
	private Company company;

	public Employee(Long empId, String empNo, String empName) {
		super();
		this.empId = empId;
		this.empNo = empNo;
		this.empName = empName;
	}

	public Employee(Long empId, String empNo, String empName, Company company) {
		super();
		this.empId = empId;
		this.empNo = empNo;
		this.empName = empName;
		this.company = company;
	}

	public Employee() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "empId", unique = true, nullable = false)
	public Long getEmpId() {
		return empId;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	@Column(name = "empNo", length = 20)
	public String getEmpNo() {
		return empNo;
	}

	public void setEmpNo(String empNo) {
		this.empNo = empNo;
	}

	@Column(name = "empName", length = 255)
	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	// Annotation @ManyToOne thực hiện mapping many to one,
	// nhiều đối tượng Employee sẽ cùng thuộc 1 đối tượng Company
	// Annotation @JoinColumn chỉ rõ thực hiện mapping qua field nào
	// (ở đây là mapping qua column companyId trong table employee)

	@ManyToOne
	@JoinColumn(name = "companyId", nullable = false)
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

}
