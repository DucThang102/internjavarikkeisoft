package com.ducthang.webservice.service;

import java.util.List;

import com.ducthang.webservice.entities.Employee;

public interface EmployeeService {
	public Employee getEmployee(Long empId);

	public List<Employee> getAllEmployees();

	public Employee addEmployee(Employee employee);

	public Employee updateEmployee(Employee employee);

	public void deleteEmployee(Long empId);
}
