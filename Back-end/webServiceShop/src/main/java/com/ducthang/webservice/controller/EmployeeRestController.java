package com.ducthang.webservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.entities.Employee;
import com.ducthang.webservice.service.EmployeeServiceImpl;

@RestController
@RequestMapping("/rest")
public class EmployeeRestController {

	@Autowired
	private EmployeeServiceImpl employeeService;

	/**
	 * get employee
	 * 
	 * @param empId
	 * @return
	 */
	@RequestMapping(value = "/employees/{empId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Employee getEmployee(@PathVariable("empId") Long empId) {
		return employeeService.getEmployee(empId);
	}

	/**
	 * get all employees
	 * 
	 * @return
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Employee> getAllEmployees() {
		return employeeService.getAllEmployees();
	}

	/**
	 * add new a employee
	 * 
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Employee addEmployee(@RequestBody Employee employee) {
		return employeeService.addEmployee(employee);
	}

	/**
	 * update one employee
	 * 
	 * @param employee
	 * @return
	 */
	@RequestMapping(value = "/employees", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Employee updateEmployee(@RequestBody Employee employee) {
		return employeeService.updateEmployee(employee);
	}

	/**
	 * delete one employee
	 * @param empId
	 * @return
	 */
	@RequestMapping(value = "/employees/{empId}", method = RequestMethod.DELETE)
	public String deleteEmployee(@PathVariable("empId") Long empId) {
		employeeService.deleteEmployee(empId);
		return "delete successful";
	}

}
