package com.ducthang.webservice.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.dao.EmployeeDAO;
import com.ducthang.webservice.entities.Employee;

@Service // thông báo thuộc layer service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired // spring boot sẽ tự tạo đối tượng EmployeeDAO trong service
	private EmployeeDAO employeeDAO;

	public Employee getEmployee(Long empId) {
		return employeeDAO.getEmployee(empId);
	}

	public List<Employee> getAllEmployees() {
		return employeeDAO.getAllEmployees();
	}

	public Employee addEmployee(Employee employee) {
		return employeeDAO.addEmployee(employee);
	}
	
	public Employee updateEmployee(Employee employee) {
		return employeeDAO.updateEmployee(employee);
	}
	
	public void deleteEmployee(Long empId) {
		employeeDAO.deleteEmployee(empId);
	}
}
