package com.ducthang.webservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	private JavaMailSender mailSender;

	@Override
	@Async // không cần chờ quá trình gửi mail hoàn tất trước khi tiếp tục với tác vụ khác
	public void sendEmail(SimpleMailMessage email) {
		mailSender.send(email);
	}

}
