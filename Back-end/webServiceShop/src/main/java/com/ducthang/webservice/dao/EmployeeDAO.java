package com.ducthang.webservice.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.entities.Employee;

@Repository // thông báo thuộc layer dao
public class EmployeeDAO {
	@Autowired
	private SessionFactory sessionFactory;

	public List<Employee> getAllEmployees() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Employee> list = session.createQuery("FROM Employee").list();
		return list;
	}

	public Employee getEmployee(Long empId) {
		Session session = sessionFactory.getCurrentSession();
		return session.get(Employee.class, empId);
	}

	public Employee addEmployee(Employee employee) {
		Session session = sessionFactory.getCurrentSession();
		session.save(employee);
		return employee;
	}


	public Employee updateEmployee(Employee employee) {
		Session session = sessionFactory.getCurrentSession();
		Employee updateEmp =session.get(Employee.class, employee.getEmpId());
		
		if (updateEmp != null) {
			updateEmp.setEmpNo(employee.getEmpNo());
			updateEmp.setEmpName(employee.getEmpName());
			updateEmp.setCompany(employee.getCompany());
			session.update(updateEmp);
		}
		
		return updateEmp;
	}

	public void deleteEmployee(Long empId) {
		Session session = sessionFactory.getCurrentSession();
		Employee emp =session.get(Employee.class,empId);
		
		session.delete(emp);
	}
}
