package com.ducthang.webservice.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/rest")
public class UploadFileRestController {

	public static String UPLOAD_DIR = "uploads";

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
		try {
			String fileName = file.getOriginalFilename();
			LocalDateTime dateTime = LocalDateTime.now();
			String dateTimeNow = DateTimeFormatter.ofPattern("ddMMYYY_HHmmss").format(dateTime);
			fileName = dateTimeNow + "_" + fileName;
			String path = request.getServletContext().getRealPath("") + UPLOAD_DIR + File.separator + fileName;
			saveFile(file.getInputStream(), path);
			return UPLOAD_DIR + File.separator + fileName;
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}
	}

	@RequestMapping(value = "/uploads", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> uploadFiles(@RequestParam("file[]") MultipartFile file[], HttpServletRequest request) {
		List<String> listFilePath = new ArrayList<String>();
		try {
			for (int i = 0; i < file.length; i++) {
				String fileName = file[i].getOriginalFilename();
				LocalDateTime dateTime = LocalDateTime.now();
				String dateTimeNow = DateTimeFormatter.ofPattern("ddMMYYY_HHmmss").format(dateTime);
				fileName = dateTimeNow + "_" + fileName;
				String path = request.getServletContext().getRealPath("") + UPLOAD_DIR + File.separator + fileName;
				saveFile(file[i].getInputStream(), path);
				listFilePath.add(UPLOAD_DIR + File.separator + fileName);
			}
			return listFilePath;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public void saveFile(InputStream inputStream, String path) {
		try {
			OutputStream outputStream = new FileOutputStream(new File(path));
			int read = 0;
			byte[] bytes = new byte[1024];
			while ((read = inputStream.read(bytes)) != -1) {
				outputStream.write(bytes, 0, read);
			}
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
