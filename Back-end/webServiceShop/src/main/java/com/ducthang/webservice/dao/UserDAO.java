package com.ducthang.webservice.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.entities.User;

@Repository
@Transactional
public class UserDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public User findUserByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "From User U where U.email = :email";

		Query<User> query = session.createQuery(hql, User.class);
		query.setParameter("email", email);

		List<User> list = query.list();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public User findByResetToken(String resetToken) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "From User U where U.resetToken = :resetToken";

		Query<User> query = session.createQuery(hql, User.class);
		query.setParameter("resetToken", resetToken);
		List<User> list = query.list();
		if (list.size() > 0) {
			System.out.println(list.get(0).toString());
			return list.get(0);
		} else {
			return null;
		}
	}

	public void saveUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
	}

}
