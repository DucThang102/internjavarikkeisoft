package com.ducthang.webservice.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.entities.Category;

@Repository
public class CategoryDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public List<Category> getAllCategories() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("unchecked")
		List<Category> list = session.createQuery("FROM Category").list();
		return list;
	}

}
