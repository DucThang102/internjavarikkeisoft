package com.ducthang.webservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.dao.UserDAO;
import com.ducthang.webservice.entities.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public User findUserByEmail(String email) {
		return userDAO.findUserByEmail(email);
	}

	@Override
	public User findUserByResetToken(String resetToken) {
		return userDAO.findByResetToken(resetToken);
	}

	@Override
	public void saveUser(User user) {
		userDAO.saveUser(user);
	}

}
