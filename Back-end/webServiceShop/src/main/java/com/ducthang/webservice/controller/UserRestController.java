package com.ducthang.webservice.controller;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.entities.User;
import com.ducthang.webservice.service.EmailServiceImpl;
import com.ducthang.webservice.service.UserServiceImpl;

@RestController
@RequestMapping("/rest")
public class UserRestController {

	@Autowired
	private EmailServiceImpl emailService;

	@Autowired
	private UserServiceImpl userService;

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST)
	@ResponseBody
	public String forgotPassword(@RequestParam("email") String email, HttpServletRequest request) {
		User user = userService.findUserByEmail(email);
		if (user == null) {
			return "We didn't find an account for that e-mail address!";
		} else {
			// Generate random 36-character string token for reset password
			user.setResetToken(UUID.randomUUID().toString());

			userService.saveUser(user);

			String appUrl = request.getScheme() + "://" + request.getServerName();

			// Email message
			SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
			passwordResetEmail.setTo(user.getEmail());
			passwordResetEmail.setSubject("Password Reset Request");
			passwordResetEmail.setText("To reset your password, click the link below:\n" + appUrl + "/reset?token="
					+ user.getResetToken());

			emailService.sendEmail(passwordResetEmail);

			return "A password reset link has been sent to " + user.getEmail();
		}
	}

}
