package com.ducthang.webservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.entities.Category;
import com.ducthang.webservice.service.CategoryServiceImpl;

@RestController
@RequestMapping("/rest")
public class CategoryRestController {
	
	@Autowired
	private CategoryServiceImpl categoryService;

	@RequestMapping(value = "/categories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<Category> getAllCategories() {
		return categoryService.getAllCategories();
	}
}
