package com.ducthang.webservice.service;

import java.util.List;

import com.ducthang.webservice.entities.Category;

public interface CategoryService {
	public List<Category> getAllCategories();
}
