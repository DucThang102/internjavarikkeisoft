package com.ducthang.webservice.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.dao.CategoryDAO;
import com.ducthang.webservice.entities.Category;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryDAO categoryDAO;

	public List<Category> getAllCategories() {
		return categoryDAO.getAllCategories();
	}

}
