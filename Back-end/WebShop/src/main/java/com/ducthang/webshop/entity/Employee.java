package com.ducthang.webshop.entity;

public class Employee {
	private String name;
	private int age;
	private String address;
	public Employee(String name, int age, String address) {
		super();
		this.name = name;
		this.age = age;
		this.address = address;
	}
	public Employee() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", address=" + address + "]";
	}
	
	public void start() {
		System.out.println("Init bean");
	}
	

	public void destroy() {
		System.out.println("Destroy bean");
	}
	
	
	public Employee createEmplyee() {
		return new Employee();
	}
	
	
}
