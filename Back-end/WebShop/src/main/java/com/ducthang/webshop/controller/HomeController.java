package com.ducthang.webshop.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ducthang.webshop.entity.Employee;

@Controller
public class HomeController {

	@RequestMapping(path = "/", method = RequestMethod.GET)
	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");

		List<Employee> list = new ArrayList<Employee>();
		for (int i = 0; i < 10; i++) {
			list.add(new Employee("Name " + i, i, "Address " + i));
		}
		modelAndView.addObject("listEmployee", list);
		
		return modelAndView;
	}

	@RequestMapping(path = "/homepage", method = RequestMethod.GET)
	public ModelAndView homepage(@RequestParam("id") int id) {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("homepage");
		modelAndView.addObject("id", id);
		
		return modelAndView;
	}
}
