package com.ducthang.web.medicinestore.controller.enduser;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.ducthang.web.medicinestore.entity.Category;
import com.ducthang.web.medicinestore.entity.StoreInfo;
import com.ducthang.web.medicinestore.service.CategoryService;
import com.ducthang.web.medicinestore.service.ProductService;
import com.ducthang.web.medicinestore.util.Constants;

@Controller
public class HomeController {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ProductService productService;

	@RequestMapping("/")
	public ModelAndView index(HttpSession session) {
		ModelAndView andView = new ModelAndView("user/index");
		List<Category> categories = categoryService.listCategories();
		andView.addObject("categories", categories);
		Long totalProducts = productService.getTotalProducts();
		andView.addObject("totalProducts", totalProducts);

		String url = Constants.WEB_INFO;
		RestTemplate restTemplate = new RestTemplate();
		StoreInfo info = restTemplate.getForObject(url, StoreInfo.class);
		System.out.println(info);
		session.setAttribute("StoreInfo", info);

		return andView;
	}


}
