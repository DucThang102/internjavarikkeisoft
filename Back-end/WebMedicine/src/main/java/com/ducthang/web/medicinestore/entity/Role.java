package com.ducthang.web.medicinestore.entity;

public class Role {
	private Integer roleID;
	private String name;

	public Role() {
		super();
	}

	public Role(Integer roleID) {
		super();
		this.roleID = roleID;
	}

	public Integer getRoleID() {
		return roleID;
	}

	public void setRoleID(Integer roleID) {
		this.roleID = roleID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Role [roleID=" + roleID + ", name=" + name + "]";
	}
	
	
}
