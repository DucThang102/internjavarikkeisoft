package com.ducthang.web.medicinestore.entity;

public class StoreInfo {
	private int id;

	private String name;

	private String address;

	private String email;

	private String facebook;

	private String about;

	private int phoneNum1;

	private String phoneNumOwner1;

	private int phoneNum2;

	private String phoneNumOwner2;

	private int latitude;

	private String longitude;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public int getPhoneNum1() {
		return phoneNum1;
	}

	public void setPhoneNum1(int phoneNum1) {
		this.phoneNum1 = phoneNum1;
	}

	public String getPhoneNumOwner1() {
		return phoneNumOwner1;
	}

	public void setPhoneNumOwner1(String phoneNumOwner1) {
		this.phoneNumOwner1 = phoneNumOwner1;
	}

	public int getPhoneNum2() {
		return phoneNum2;
	}

	public void setPhoneNum2(int phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}

	public String getPhoneNumOwner2() {
		return phoneNumOwner2;
	}

	public void setPhoneNumOwner2(String phoneNumOwner2) {
		this.phoneNumOwner2 = phoneNumOwner2;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "StoreInfo [id=" + id + ", name=" + name + ", address=" + address + ", email=" + email + ", facebook="
				+ facebook + ", about=" + about + ", phoneNum1=" + phoneNum1 + ", phoneNumOwner1=" + phoneNumOwner1
				+ ", phoneNum2=" + phoneNum2 + ", phoneNumOwner2=" + phoneNumOwner2 + ", latitude=" + latitude
				+ ", longitude=" + longitude + "]";
	}
	
	

}
