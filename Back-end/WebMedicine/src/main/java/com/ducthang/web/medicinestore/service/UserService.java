package com.ducthang.web.medicinestore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.web.medicinestore.dao.UserDAO;
import com.ducthang.web.medicinestore.entity.User;
import com.ducthang.web.medicinestore.interfaces.IUser;

@Service
public class UserService implements IUser {
	
	@Autowired
	private UserDAO userDAO;

	public User login(String email, String password) {
		return userDAO.login(email, password);
	}

	public String register(User user) {
		return userDAO.register(user);
	}

	public String forgotPassword(String email) {
		return userDAO.forgotPassword(email);
	}

	public String checkToken(String token) {
		return userDAO.checkToken(token);
	}

	public String resetPassword(String token, String newPassword) {
		return userDAO.resetPassword(token, newPassword);
	}

}
