package com.ducthang.web.medicinestore.interfaces;

import java.util.List;

import com.ducthang.web.medicinestore.entity.Category;

public interface ICategory {
	public List<Category> listCategories();

	public boolean addCategoty(Category category);

	public boolean removeCategory(int id);

	public boolean updateCategory(Category category);
}
