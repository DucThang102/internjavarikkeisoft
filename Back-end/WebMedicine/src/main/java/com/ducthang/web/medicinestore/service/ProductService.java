package com.ducthang.web.medicinestore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.web.medicinestore.dao.ProductDAO;
import com.ducthang.web.medicinestore.entity.Product;
import com.ducthang.web.medicinestore.interfaces.IProduct;

@Service
public class ProductService implements IProduct {
	
	@Autowired
	private ProductDAO dao;

	public List<Product> listProducts(int offset, int limit) {
		return dao.listProducts(offset, limit);
	}

	public Long getTotalProducts() {
		return dao.getTotalProducts();
	}

}
