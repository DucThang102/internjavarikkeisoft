package com.ducthang.web.medicinestore.interfaces;

import java.util.List;

import com.ducthang.web.medicinestore.entity.Product;

public interface IProduct {
	
	public List<Product> listProducts(int offset, int limit);
	
	public Long getTotalProducts();
}
