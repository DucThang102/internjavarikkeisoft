package com.ducthang.web.medicinestore.dao;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ducthang.web.medicinestore.entity.Role;
import com.ducthang.web.medicinestore.entity.User;
import com.ducthang.web.medicinestore.interfaces.IUser;
import com.ducthang.web.medicinestore.util.Constants;
import com.google.gson.Gson;

@Repository
public class UserDAO implements IUser {

	public User login(String email, String password) {
		try {
			String url = Constants.DOMAIN + Constants.LOGIN + "?email=" + email + "&password=" + password.hashCode();
			System.out.println(url);
			RestTemplate restTemplate = new RestTemplate();
			String response = restTemplate.getForObject(url, String.class);
			Gson gson = new Gson();
			return gson.fromJson(response, User.class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public String register(User user) {
		try {
			String url = Constants.DOMAIN + Constants.REGISTER;
			user.setRole(new Role(1));
			user.setPassword(user.getPassword().hashCode()+"");
			
			RestTemplate restTemplate = new RestTemplate();
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);
			
	        HttpEntity<User> requestBody = new HttpEntity<User>(user, headers);
	 
	       return restTemplate.postForObject(url, requestBody, String.class);
	        
			
		}catch(Exception e) {
			e.printStackTrace();
			return "Thất bại!";
		}
	}

	public String forgotPassword(String email) {
		try {
			String url = Constants.DOMAIN + Constants.FORGOT_PASSWORD + "?email=" + email;
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.getForObject(url, String.class);
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}
	}

	public String checkToken(String token) {
		try {
			String url = Constants.DOMAIN + Constants.CHECK_TOKEN + "?resetToken=" + token;
			RestTemplate restTemplate = new RestTemplate();
			return restTemplate.getForObject(url, String.class);
		} catch (Exception e) {
			e.printStackTrace();
			return "Error";
		}
	}

	public String resetPassword(String token, String newPassword) {
		try {
			String url = Constants.DOMAIN + Constants.RESET_PASSWORD+"?resetToken="+token+"&newPassword="+newPassword.hashCode();
			RestTemplate restTemplate = new RestTemplate();
	       return restTemplate.getForObject(url, String.class);
		}catch(Exception e) {
			e.printStackTrace();
			return "Thất bại!";
		}
	}

}
