package com.ducthang.web.medicinestore.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.web.medicinestore.dao.CategoryDAO;
import com.ducthang.web.medicinestore.entity.Category;
import com.ducthang.web.medicinestore.interfaces.ICategory;

@Service
public class CategoryService implements ICategory {

	@Autowired
	private CategoryDAO categoryDAO;
	
	public List<Category> listCategories() {
		return categoryDAO.listCategories();
	}

	public boolean addCategoty(Category category) {
		return categoryDAO.addCategoty(category);
	}

	public boolean removeCategory(int id) {
		return categoryDAO.removeCategory(id);
	}

	public boolean updateCategory(Category category) {
		return categoryDAO.updateCategory(category);
	}

}
