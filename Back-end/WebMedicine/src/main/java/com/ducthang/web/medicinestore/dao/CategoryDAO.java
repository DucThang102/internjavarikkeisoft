package com.ducthang.web.medicinestore.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ducthang.web.medicinestore.entity.Category;
import com.ducthang.web.medicinestore.interfaces.ICategory;
import com.ducthang.web.medicinestore.util.Constants;

@Repository
public class CategoryDAO implements ICategory{

	public List<Category> listCategories() {
		String url = Constants.DOMAIN + Constants.CATEGORY_GET_ALL;
		RestTemplate restTemplate = new RestTemplate();
		@SuppressWarnings("unchecked")
		List<Category> list = restTemplate.getForObject(url, List.class);
		return list;
	}

	public boolean addCategoty(Category category) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean removeCategory(int id) {
		// TODO Auto-generated method stub
		return false;
	}

	public boolean updateCategory(Category category) {
		// TODO Auto-generated method stub
		return false;
	}

}
