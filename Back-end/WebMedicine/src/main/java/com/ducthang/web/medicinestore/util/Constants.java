package com.ducthang.web.medicinestore.util;

public class Constants {
	public static final String DOMAIN = "http://localhost:8081/";
	public static final String LOGIN = "user/login";
	public static final String REGISTER = "user/register";
	public static final String FORGOT_PASSWORD = "user/forgotPassword";
	public static final String CHECK_TOKEN = "user/checkResetToken";
	public static final String RESET_PASSWORD = "user/resetPassword";
	public static final String CATEGORY_GET_ALL = "/category/getAll";
	public static final String PRODUCT_GET_ALL = "/product/getAll";
	public static final String TOTAL_PRODUCT = "/product/totalProducts";
	public static final String WEB_INFO = DOMAIN + "/store-info/get";
}
