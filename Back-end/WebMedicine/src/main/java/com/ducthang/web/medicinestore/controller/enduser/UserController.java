package com.ducthang.web.medicinestore.controller.enduser;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ducthang.web.medicinestore.entity.User;
import com.ducthang.web.medicinestore.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService service;

	@RequestMapping("/login-page")
	public ModelAndView loginPage() {
		ModelAndView andView = new ModelAndView("login");
		return andView;
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ModelAndView login(@RequestParam("email") String email, @RequestParam("password") String password, HttpSession session) {

		ModelAndView modelAndView = new ModelAndView();

		User user = service.login(email, password);
		System.out.println(user);
		if (user != null) {
			session.setAttribute("user", user);
			modelAndView.setViewName("user/index");
		} else {
			modelAndView.addObject("status", "Tài khoản không chính xác!");
			modelAndView.setViewName("login");
		}
		return modelAndView;
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public ModelAndView logout(HttpSession session) {
		ModelAndView modelAndView = new ModelAndView("user/index");
		session.removeAttribute("user");
		return modelAndView;
	}

	@RequestMapping(value = "/register-page", method = RequestMethod.GET)
	public ModelAndView register() {
		ModelAndView modelAndView = new ModelAndView("register");
		return modelAndView;
	}

	@RequestMapping(value = "register", method = RequestMethod.POST)
	public ModelAndView register(@ModelAttribute("userForm") User user) {
		ModelAndView modelAndView = new ModelAndView();
		
		String status = service.register(user);
		
		if (status.equals("Thành công!")) {
			modelAndView.setViewName("login");
		}else {
			modelAndView.setViewName("register");
		}
		modelAndView.addObject("status", status);
		return modelAndView;
	}
	
	@GetMapping(value = "forgot-password")
	public ModelAndView forgotPassword() {
		
		return new ModelAndView("forgot-password");
	}
	
	
	
	@PostMapping(value = "forgot-password")
	public ModelAndView forgotPassword(@RequestParam("email") String email) {
		ModelAndView modelAndView = new ModelAndView();
		
		String rs = service.forgotPassword(email);
		modelAndView.addObject("status", rs);
		if (rs.equals("Không tìm thấy tài khoản nào có email này!")) {
			modelAndView.setViewName("forgot-password");
		}else {
			modelAndView.setViewName("reset-password");
		}
		return modelAndView;
	}
	
	@PostMapping(value = "check-token")
	@ResponseBody
	public String checkToken(@RequestParam("token") String token, HttpSession session) {
		session.setAttribute("token", token);
		return service.checkToken(token);
	}
	
	
	@GetMapping("a")
	public ModelAndView a() {
		return new ModelAndView("reset-password");
	}
	
	
	@PostMapping("reset-password")
	public ModelAndView resetPassword(@RequestParam("newPassword") String newPassword, HttpSession session) {
		String token = (String) session.getAttribute("token");
		ModelAndView modelAndView = new ModelAndView();
		String status = service.resetPassword(token, newPassword);
		modelAndView.addObject("status", status);
		if (status.equals("Thành công !")) {
			modelAndView.setViewName("login");
			modelAndView.addObject("status", "Đổi mật khẩu thành công! Mời bạn đăng nhập");
		}else {
			modelAndView.setViewName("reset-password");
		}
		return modelAndView;
	}
}
