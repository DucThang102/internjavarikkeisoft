package com.ducthang.web.medicinestore.interfaces;

import com.ducthang.web.medicinestore.entity.User;

public interface IUser {
	public User login(String email, String password);

	public String register(User user);
	
	public String forgotPassword(String email);
	
	public String checkToken(String token);
	
	public String resetPassword(String token, String newPassword);
}
