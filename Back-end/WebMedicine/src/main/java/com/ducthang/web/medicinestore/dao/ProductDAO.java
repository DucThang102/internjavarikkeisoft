package com.ducthang.web.medicinestore.dao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.ducthang.web.medicinestore.entity.Product;
import com.ducthang.web.medicinestore.interfaces.IProduct;
import com.ducthang.web.medicinestore.util.Constants;

@Repository
public class ProductDAO implements IProduct{

	
	public List<Product> listProducts(int offset, int limit) {
		String url = Constants.DOMAIN + Constants.PRODUCT_GET_ALL +"?limit="+limit +"&offset="+offset;
		RestTemplate restTemplate = new RestTemplate();
		@SuppressWarnings("unchecked")
		List<Product> list = restTemplate.getForObject(url, List.class);
		return list;
	}

	public Long getTotalProducts() {
		String url = Constants.DOMAIN + Constants.TOTAL_PRODUCT;
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(url, Long.class);
	}

}
