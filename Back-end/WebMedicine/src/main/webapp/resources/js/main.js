$(document).ready(function() {
	pagination(totalProducts);
});

function pagination(totalProducts) {
	var pageSize = 9;
	var totalPage = Math.ceil(totalProducts / pageSize)
	
	
	 if ($('#pagination a').length === 0 ) {
            $('#pagination').empty();
            $('#pagination').removeData("twbs-pagination");
            $('#pagination').unbind("page");
        }
	
	$('#pagination').twbsPagination({
		totalPages : totalPage,
		visiblePages : 3,
	    first: "Đầu",
        next: "Tiếp",
        last: "Cuối",
        prev: "Trước",
		onPageClick : function(event, page) {
			loadData((page -1 )*pageSize,pageSize);
		}
	});
}

function loadData(ofset , limit) {
	$.ajax({
		url: "http://localhost:8081/product/getAll?limit="+limit+"&offset="+ofset,

		success: function (data) {
			console.log(data);
			var html = ``;
			data.forEach(item => {
				html += (`
					<div class="col-md-4 col-sm-4" style="margin-top: 30px">

					<div class="product_item">

						<div class="product-image">
							<a href=""><img width="60" height="100"
								src="`); if(item.images.length > 0){ html += `${item.images[0].link }`}  html+=(`"  alt="" class=""></a>
						</div>

						<div>
							<p>
								<a href=""
									class="product_name">${item.name}</a>
							</p>
						</div>

						<span class='price text-right'>${item.price } </span>
						
						<div>
							<button onclick="addCart()" class='btn btn-info'>
								<span class="glyphicon glyphicon-shopping-cart"
									aria-hidden="true"></span> Thêm giỏ hàng
							</button>
						</div>
					</div>

				</div>
				`);
			});
			
			$("#listProduct").html(html);
			pagination(totalProducts);

		}
	});
}

function checkResetToken() {
	var token = $("#token").val();
	$.ajax({
		type : "POST",
		url : "check-token?token=" + token,
		type : "POST",
		async : true,
		processData : false,
		cache : false,
		success : function(data) {
			if (data == "true") {
				$(".password-form").css("display", "block");
				$(".token-form").css("display", "none");
				$(".status").text("Mời nhập mật khẩu mới ");
			} else {
				$(".status").text(
						"Mã token không chính xác hoặc đã hết hiệu lực");
			}
		},
		error : function(xhr, ajaxOptions, thrownError) {
			console.log(xhr.status);
		}
	});
}

function validateNewPassword() {
	var newPassword = $("#newPassword").val();
	var confirmPasss = $("#confirm_pass").val();
	if (newPassword.length != 8) {
		alert("Mật khẩu phải dài 8 ký tự!");
		return false;
	} else if (newPassword != confirmPasss) {
		alert("Mật khẩu không khớp!");
		return false;
	} else {
		return true;
	}
}


