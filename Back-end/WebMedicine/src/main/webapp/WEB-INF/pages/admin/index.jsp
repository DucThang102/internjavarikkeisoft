<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
<!-- Required meta tags-->
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="au theme template">
<meta name="keywords" content="au theme template">

<!-- Title Page-->
<title>Trang quản trị</title>
<link rel="shortcut icon" href="resources/image/logo.jpg">

<!-- Fontfaces CSS-->
<link href="../resources/admin/css/font-face.css" rel="stylesheet"
	media="all">
<link
	href="../resources/admin/vendor/font-awesome-4.7/css/font-awesome.min.css"
	rel="stylesheet" media="all">
<link
	href="../resources/admin/vendor/font-awesome-5/css/fontawesome-all.min.css"
	rel="stylesheet" media="all">
<link
	href="../resources/admin/vendor/mdi-font/css/material-design-iconic-font.min.css"
	rel="stylesheet" media="all">

<!-- Bootstrap CSS-->
<link href="../resources/admin/vendor/bootstrap-4.1/bootstrap.min.css"
	rel="stylesheet" media="all">

<!-- Vendor CSS-->
<link href="../resources/admin/vendor/animsition/animsition.min.css"
	rel="stylesheet" media="all">
<link
	href="../resources/admin/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css"
	rel="stylesheet" media="all">
<link href="../resources/admin/vendor/wow/animate.css" rel="stylesheet"
	media="all">
<link href="../resources/admin/vendor/css-hamburgers/hamburgers.min.css"
	rel="stylesheet" media="all">
<link href="../resources/admin/vendor/slick/slick.css" rel="stylesheet"
	media="all">
<link href="../resources/admin/vendor/select2/select2.min.css"
	rel="stylesheet" media="all">
<link
	href="../resources/admin/vendor/perfect-scrollbar/perfect-scrollbar.css"
	rel="stylesheet" media="all">

<!-- Main CSS-->
<link href="../resources/admin/css/theme.css" rel="stylesheet"
	media="all">

</head>

<body class="animsition">
	<div class="page-wrapper">
		<!-- HEADER MOBILE-->
		<header class="header-mobile d-block d-lg-none">
			<div class="header-mobile__bar">
				<div class="container-fluid">
					<div class="header-mobile-inner">
						<a class="logo" href=""> ${StoreInfo.name} </a>
						<button class="hamburger hamburger--slider" type="button">
							<span class="hamburger-box"> <span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
			</div>
			<nav class="navbar-mobile">
				<div class="container-fluid">
					<ul class="navbar-mobile__list list-unstyled">
						<li class="active has-sub"><a class="js-arrow" href="#">
								<i class="fas fa-tachometer-alt"></i>Danh mục
						</a></li>
						<li><a href=""> <i class="fas fa-chart-bar"></i>Sản phẩm
						</a></li>
						<li><a href=""> <i class="fas fa-table"></i>Khách hàng
						</a></li>
						<li class="has-sub"><a class="js-arrow" href="#"> <i
								class="fas fa-copy"></i>Đơn hàng
						</a>
							<ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
								<li><a href="">Đã xác nhận</a></li>
								<li><a href="">Đơn hàng mới</a></li>
							</ul></li>
						<li><a href="#"> <i class="fas fa-calendar-alt"></i>Thông
								tin web
						</a></li>

					</ul>
				</div>
			</nav>
		</header>
		<!-- END HEADER MOBILE-->

		<!-- MENU SIDEBAR-->
		<aside class="menu-sidebar d-none d-lg-block">
			<div class="logo">
				<a href="#">${StoreInfo.name} </a>
			</div>
			<div class="menu-sidebar__content js-scrollbar1">
				<nav class="navbar-sidebar">
					<ul class="list-unstyled navbar__list">
						<li class="active has-sub"><a class="js-arrow" href="#">
								<i class="fas fa-tachometer-alt"></i>Danh mục
						</a></li>
						<li><a href=""> <i class="fas fa-chart-bar"></i>Sản phẩm
						</a></li>
						<li><a href=""> <i class="fas fa-table"></i>Khách hàng
						</a></li>

						<li class="has-sub"><a class="js-arrow" href="#"> <i
								class="fas fa-copy"></i>Đơn hàng
						</a>
							<ul class="list-unstyled navbar__sub-list js-sub-list">
								<li><a href="">Đã xác nhận</a></li>
								<li><a href="">Đơn hàng mới</a></li>
							</ul></li>

						<li><a href="#"> <i class="fas fa-calendar-alt"></i>Thông
								tin web
						</a></li>

					</ul>
				</nav>
			</div>
		</aside>
		<!-- END MENU SIDEBAR-->

		<!-- PAGE CONTAINER-->
		<div class="page-container">
			<!-- HEADER DESKTOP-->
			<header class="header-desktop">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="header-wrap">
							<form class="form-header" action="" method="POST">
								<input class="au-input au-input--xl" type="text" name="search"
									placeholder="Tìm kiếm" />
								<button class="au-btn--submit" type="submit">
									<i class="zmdi zmdi-search"></i>
								</button>
							</form>
							<div class="header-button">
								<div class="noti-wrap">
									<div class="noti__item js-item-menu">
										<i class="zmdi zmdi-notifications"></i> <span class="quantity">3</span>
										<div class="notifi-dropdown js-dropdown">
											<div class="notifi__title">
												<p>You have 3 Notifications</p>
											</div>
											<div class="notifi__item">
												<div class="bg-c1 img-cir img-40">
													<i class="zmdi zmdi-email-open"></i>
												</div>
												<div class="content">
													<p>You got a email notification</p>
													<span class="date">April 12, 2018 06:50</span>
												</div>
											</div>
											<div class="notifi__item">
												<div class="bg-c2 img-cir img-40">
													<i class="zmdi zmdi-account-box"></i>
												</div>
												<div class="content">
													<p>Your account has been blocked</p>
													<span class="date">April 12, 2018 06:50</span>
												</div>
											</div>
											<div class="notifi__item">
												<div class="bg-c3 img-cir img-40">
													<i class="zmdi zmdi-file-text"></i>
												</div>
												<div class="content">
													<p>You got a new file</p>
													<span class="date">April 12, 2018 06:50</span>
												</div>
											</div>
											<div class="notifi__footer">
												<a href="#">All notifications</a>
											</div>
										</div>
									</div>
								</div>
								<div class="account-wrap">
									<div class="account-item clearfix js-item-menu">
										<div class="content">
											<a class="js-acc-btn" href="#">${sessionScope.user.name}</a>
										</div>
										<div class="account-dropdown js-dropdown">
											<div class="info clearfix">
												<div class="">
													<h5 class="name">
														<a href="#">${sessionScope.user.name}</a>
													</h5>
													<span class="email">${sessionScope.user.email}</span>
												</div>
											</div>
											<div class="account-dropdown__body">
												<div class="account-dropdown__item">
													<a href="#"> <i class="zmdi zmdi-account"></i>Cập nhật
														thông tin
													</a>
												</div>
											</div>
											<div class="account-dropdown__footer">
												<a href="#"> <i class="zmdi zmdi-power"></i>Đăng xuất
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- END HEADER DESKTOP-->

			<!-- MAIN CONTENT-->
			<div class="main-content">
				<div class="section__content section__content--p30">
					<div class="container-fluid">
						<div class="row">
							<div class="table-responsive table--no-card m-b-30">
								<table
									class="table table-borderless table-striped table-earning">
									<thead>
										<tr>
											<th>STT</th>
											<th>Mã Danh mục</th>
											<th>Tên Danh mục</th>
											<th class="text-right">Quản lý</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>100398</td>
											<td>iPhone X 64Gb Grey</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>2</td>
											<td>100397</td>
											<td>Samsung S8 Black</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>3</td>
											<td>100396</td>
											<td>Game Console Controller</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>4</td>
											<td>100395</td>
											<td>iPhone X 256Gb Black</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>5</td>
											<td>100393</td>
											<td>USB 3.0 Cable</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>6</td>
											<td>100396</td>
											<td>Game Console Controller</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>7</td>
											<td>100395</td>
											<td>iPhone X 256Gb Black</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
										<tr>
											<td>8</td>
											<td>100393</td>
											<td>USB 3.0 Cable</td>
											<td>
												<div class="table-data-feature">
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Edit">
														<i class="zmdi zmdi-edit"></i>
													</button>
													<button class="item" data-toggle="tooltip"
														data-placement="top" title="Delete">
														<i class="zmdi zmdi-delete"></i>
													</button>
													
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END MAIN CONTENT-->
			<!-- END PAGE CONTAINER-->
		</div>

	</div>

	<!-- Jquery JS-->
	<script src="../resources/admin/vendor/jquery-3.2.1.min.js"></script>
	<!-- Bootstrap JS-->
	<script src="../resources/admin/vendor/bootstrap-4.1/popper.min.js"></script>
	<script src="../resources/admin/vendor/bootstrap-4.1/bootstrap.min.js"></script>
	<!-- Vendor JS       -->
	<script src="../resources/admin/vendor/slick/slick.min.js">
		
	</script>
	<script src="../resources/admin/vendor/wow/wow.min.js"></script>
	<script src="../resources/admin/vendor/animsition/animsition.min.js"></script>
	<script
		src="../resources/admin/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
		
	</script>
	<script
		src="../resources/admin/vendor/counter-up/jquery.waypoints.min.js"></script>
	<script
		src="../resources/admin/vendor/counter-up/jquery.counterup.min.js">
		
	</script>
	<script
		src="../resources/admin/vendor/circle-progress/circle-progress.min.js"></script>
	<script
		src="../resources/admin/vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
	<script src="../resources/admin/vendor/chartjs/Chart.bundle.min.js"></script>
	<script src="../resources/admin/vendor/select2/select2.min.js">
		
	</script>

	<!-- Main JS-->
	<script src="../resources/admin/js/main.js"></script>
</body>

</html>
<!-- end document-->
