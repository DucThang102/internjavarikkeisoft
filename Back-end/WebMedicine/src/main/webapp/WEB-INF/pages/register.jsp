<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Đăng ký tài khoản</title>
    <!-- plugins:css -->
    <link href="resources/css/materialdesignicons.min.css" rel="stylesheet" />
    <link href="resources/css/vendor.bundle.base.css" rel="stylesheet" />
    <link href="resources/css/vendor.bundle.addons.css" rel="stylesheet" />
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->

    <link href="resources/css/style_login.css" rel="stylesheet" />
    <!-- endinject -->
    <link rel="shortcut icon" href="resources/image/logo.jpg" />
</head>
<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper auth-page">
            <div class="content-wrapper d-flex align-items-center auth register-bg-1 theme-one">
                <div class="row w-100">
                    <div class="col-lg-4 mx-auto">
                        <div class="auto-form-wrapper">
                        
                        <span style="color:red">${status}</span>
                            <form action="register" method="post" name="userForm" onsubmit="return validateForm()" accept-charset="UTF-8">
                                <label class="label">Email</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="email" placeholder="Email">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <label class="label">Mật khẩu</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control" name="password" maxlength="8" required placeholder="Mật khẩu">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <label class="label">Nhập lại mật khẩu</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="password" class="form-control" name="confirm_pass" maxlength="8" required placeholder="Nhập lại mật khẩu">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <label class="label">Họ và tên</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" required placeholder="Họ và tên">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                
                                <label class="label">Ngày sinh</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="date" class="form-control" name="dateOfBirth" required placeholder="Ngày sinh">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <label class="label">Số điện thoại</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="phone" required placeholder="Số điện thoại">
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="mdi mdi-check-circle-outline"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                              
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary submit-btn btn-block">Đăng ký</button>
                                </div>
                                <div class="text-block text-center my-3">
                                    <span class="text-small font-weight-semibold">Tôi đã có tài khoản ?</span>
                                    <a href="login-page" class="text-black text-small">Đăng nhập</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <script src="resources/js/jquery-3.3.1.min.js"></script>
    <!-- plugins:js -->
    <script src="resources/js/vendor.bundle.addons.js"></script>
    <script src="resources/js/vendor.bundle.base.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="resources/js/off-canvas.js"></script>
    <script src="resources/js/misc.js"></script>
    <!-- endinject -->
    <script src="resources/js/email-validation.js"></script>
    <script>
        function validateForm() {
            var SDT = document.forms["userForm"]["phone"].value;
            var Email = document.forms["userForm"]["email"].value;
            var MatKhau = document.forms["userForm"]["password"].value;
            var confirm_pass = document.forms["userForm"]["confirm_pass"].value;

            if (SDT.length != 10) {
                alert("Số điện thoại không đúng");
                return false;
            } else if (!ValidateEmail(Email)) {
                alert("Email không đúng");
                return false;
            } else if (MatKhau.length != 8) {
                alert("Mật khẩu dài 8 kí tự ");
                return false;
            } else if (MatKhau != confirm_pass) {
                alert("Mật khẩu không khớp");
                return false;
            }
        }

    </script>
</body>
</html>