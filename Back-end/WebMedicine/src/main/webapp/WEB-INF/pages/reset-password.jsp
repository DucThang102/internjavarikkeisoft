<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Đặt lại mật khẩu</title>
<!-- plugins:css -->
<link href="resources/css/materialdesignicons.min.css" rel="stylesheet" />
<link href="resources/css/vendor.bundle.base.css" rel="stylesheet" />
<link href="resources/css/vendor.bundle.addons.css" rel="stylesheet" />
<link href="resources/css/style_login.css" rel="stylesheet" />
<link rel="shortcut icon" href="resources/image/logo.jpg" />

</head>
<body>
	<div class="container-scroller">
		<div
			class="container-fluid page-body-wrapper full-page-wrapper auth-page">
			<div
				class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
				<div class="row w-100">
					<div class="col-lg-4 mx-auto">
						<div class="auto-form-wrapper">

							<div>
								<span class="status" style="color: blue">${status}</span>
							</div>
							<div class="token-form">
<!-- 								<form action onsubmit="checkResetToken()"> -->
									<div class="form-group">
										<label class="label">Nhập mã token nhận được từ email</label>
										<div class="form-group">
											<div class="input-group">
												<input type="text" class="form-control" id="token" name="token"
													required placeholder="token">
												<div class="input-group-append">
													<span class="input-group-text"> <i
														class="mdi mdi-check-circle-outline"></i>
													</span>
												</div>
											</div>
										</div>
										<div class="form-group">
											<button type="submit"  onclick="checkResetToken()"
												class="btn btn-primary submit-btn btn-block">Xác
												nhận</button>
										</div>
									</div>
<!-- 								</form> -->
							</div>
							<div class="password-form">
								<form action="reset-password" onsubmit="return validateNewPassword()" method="post">

									<div class="form-group">
										<label class="label">Mật khẩu</label>
										<div class="form-group">
											<div class="input-group">
												<input type="password" id="newPassword" class="form-control" name="newPassword"
													maxlength="8" required placeholder="Mật khẩu">
												<div class="input-group-append">
													<span class="input-group-text"> <i
														class="mdi mdi-check-circle-outline"></i>
													</span>
												</div>
											</div>
										</div>
									</div>
									<label class="label">Nhập lại mật khẩu</label>
									<div class="form-group">
										<div class="input-group">
											<input type="password" id="confirm_pass" class="form-control"
												name="confirm_pass" maxlength="8" required
												placeholder="Nhập lại mật khẩu">
											<div class="input-group-append">
												<span class="input-group-text"> <i
													class="mdi mdi-check-circle-outline"></i>
												</span>
											</div>
										</div>
									</div>


									<div class="form-group">
										<button type="submit"
											class="btn btn-primary submit-btn btn-block">Đặt lại
											mật khẩu</button>
									</div>
								</form>
							</div>

							<div class="text-block text-center my-3">
								<span class="text-small font-weight-semibold">Chưa có tài
									khoản ?</span> <a href="register-page" class="text-black text-small">Đăng
									kí tài khoản mới</a>
							</div>

							<div class="text-block text-center my-3">
								<span> <a href="login-page" class="text-black text-small">Đăng
										nhập</a>
								</span> <span> <a href="<%=request.getContextPath()%>"
									class="text-black text-small"><b> Trang chủ</b></a>
								</span>
							</div>

						</div>

					</div>
				</div>
			</div>
			<!-- content-wrapper ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->
	<script src="resources/js/jquery-3.3.1.min.js"></script>
	<!-- plugins:js -->
	<script src="resources/js/vendor.bundle.addons.js"></script>
	<script src="resources/js/vendor.bundle.base.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script src="resources/js/off-canvas.js"></script>
	<script src="resources/js/misc.js"></script>
	<!-- endinject -->

	<script src="resources/js/main.js"></script>
</body>
</html>