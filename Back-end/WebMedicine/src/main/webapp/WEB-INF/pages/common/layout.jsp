<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE html>
<html>
<head>

<title>thuốc thú cưng hải hiền</title>
<link rel="shortcut icon" href="resources/image/logo.jpg">
<link rel="stylesheet" href="resources/css/bootstrap.min.css">
<link rel="stylesheet" href="resources/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="resources/css/owl.carousel.min.css">
<link rel="stylesheet" href="resources/css/owl.theme.default.min.css">
<link href="resources/css/alertifyjs/alertify.min.css" rel="stylesheet" />
<link href="resources/css/alertifyjs/themes/default.min.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
	integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay"
	crossorigin="anonymous">
<link
	href="resources/css/footer-distributed-with-address-and-phones.css"
	rel="stylesheet" />
<link href="resources/css/style_end_user.css" rel="stylesheet" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Index</title>
<body>

	<%@ include file="../common/header.jsp"%>
	<%@ include file="../common/navMenu.jsp"%>

	<div class="main">
		<div class="container clearfix">
			<%@ include file="../common/leftMenu.jsp"%>

			<div class="col-md-9 col-sm-12 col-xs-12">//content here</div>
		</div>
	</div>

	<%@ include file="../common/footer.jsp"%>
	<script src="resources/js/jquery-3.3.1.min.js"></script>
</body>
</html>