<%@ page contentType="text/html;charset=UTF-8" language="java" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="nav">
	<div class="container">
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul style="margin: 0;" class="main-menu col-md-8">
				<li><a href="">Trang chủ</a></li>
				<li><a href="">Sản phẩm</a>
					<ul class="product-menu">
						<c:forEach var="category" items="${categories}">
							<li><a href="#"> ${category.name}</a></li>
						</c:forEach>
					</ul></li>
				<li><a href="">Giới thiệu</a></li>
			</ul>

			<div class="col-md-4 hidden-sm hidden-xs hotline">
				<img src="resources/image/call_white.png" width="30" alt="hotline">
				<span style="color: white;">hotline :</span> 
				<a href="">${StoreInfo.phoneNum1}</a> 
					<a href="">- ${StoreInfo.phoneNum2}</a>
			</div>
		</div>
		<!-- /.navbar-collapse -->

	</div>
</div>
