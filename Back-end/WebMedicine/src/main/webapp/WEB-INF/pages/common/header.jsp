<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="header">
	<div class="container">
		<div class="logo">
			<div class="col-md-2 col-sm-2">
				<a href=""><img src="resources/image/logo.jpg" alt=""></a>
			</div>
			<div class="col-md-5 col-sm-5 hidden-xs search">
				<form action="" method="post" class="search-form">
					<input type="text" name="contentSearch" value=""
						placeholder="Nhập từ khóa tìm kiếm" class="search-input">
					<input type="submit" value="Tìm kiếm " class="search-btn">
				</form>
			</div>
			<div class="col-md-2 col-sm-2 hidden-xs sign-in"
				style="text-align: center; margin-top: 10px">

				<img style="width: 60px;" src="resources/image/user.png" alt="">
<%-- 				<c:set var="user" scope="session" value="${sessionScope.user}" /> --%>
				<c:choose>
					<c:when test="${sessionScope.user != null }">
						<div>
							<span style="color: forestgreen;"> ${sessionScope.user.name} <a href="logout">
									<span title="Đăng xuất" class="glyphicon glyphicon-log-out"></span>
							</a>
							</span>
						</div>
					</c:when>
					<c:otherwise>
						<a href="login-page">
							<p>Đăng nhập</p>
						</a>
					</c:otherwise>
				</c:choose>

			</div>

			<div class="col-md-2 col-sm-2 hidden-xs cart"
				style="text-align: center; margin-top: 10px">
				<a href=""> <img style="width: 60px;"
					src="resources/image/cart.png" alt="">
					<p>Giỏ hàng</p>
				</a>
				<div id="item_quantity" class="item_quantity">0</div>
			</div>

		</div>

		<div style="text-align: center;" class="hidden-lg hidden-md hidden-sm">
			<h1>
				<a style="font-size: 20px; color: green" href="user/index">Thuốc
					thứ cưng hải hiền</a>
			</h1>
		</div>
	</div>
	<div style="background: green"
		class="mobile-menu hidden-lg hidden-md hidden-sm">
		<span style="padding-left: 10px;" class="mobile-cart"> <a
			href="/ShoppingCart/Index"><img
				src="resources/image/cart-white.png" alt=""></a>
		</span>
		 <span class="mobile-cart"> <c:choose>
				<c:when test="${sessionScope.user != null }">
					<a style="color: white; margin-left: 60px">
						${sessionScope.user.name} </a>
					<a style="color: white;" href="logout"> <span
						class="glyphicon glyphicon-log-out"></span></a>
				</c:when>
				<c:otherwise>
					<a href="login-page" style="color: white; margin-left: 60px">
						Đăng nhập </a>
				</c:otherwise>
			</c:choose>
		</span>

		<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".navbar-ex1-collapse">
			<span class="sr-only">toggle navigation</span> <span class="icon-bar"></span>
			<span class="icon-bar"></span> <span class="icon-bar"></span>
		</button>
	</div>

</div>