<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="col-md-9 col-sm-12 col-xs-12">
	<div class="selling-products">
		<div class="title">
			<h3>Sản phẩm</h3>
		</div>

		<div class="clearfix" id="listProduct">
			<c:set var="totalProducts" scope="session" value="${totalProducts }" />

		</div>
		<div style="justify-content: center">
			<ul id="pagination" class="pagination-sm"></ul>
		</div>

	</div>

</div>