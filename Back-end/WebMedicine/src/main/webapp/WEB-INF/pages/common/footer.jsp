<%@ page contentType="text/html;charset=UTF-8" language="java" %> 
<footer class="footer-distributed">
    <div class="footer-left">
        <h3 id="name">${StoreInfo.name}</h3>
        <p class="footer-links">
            <a href="">Trang chủ</a>
            ·
            <a href="">Sản phẩm</a>
            ·
            <a href="">Giới thiệu</a>

        </p>
        <p class="footer-company-name">${StoreInfo.name} &copy; 2019</p>
    </div>
    <div class="footer-center">
        <div>
            <i class="fa fa-map-marker"></i>
            <p><span class="address">${StoreInfo.address}</span></p>
        </div>
        <div>
            <i class="fa fa-phone"></i>
            <p> <p class="phoneNumOwner1">${StoreInfo.phoneNumOwner1} :</p> <a class="phoneNum1" style="color:red" href="">${StoreInfo.phoneNum1} </a></p> <br />
            <i class="fa fa-phone"></i>
            <p> <p class="phoneNumOwner2">${StoreInfo.phoneNumOwner2} :</p> <a class="phoneNum2" style="color:red" href="">${StoreInfo.phoneNum2}</a></p>
        </div>
        <div>
            <i class="fa fa-envelope"></i>
            <p><a class="email" style="color:white" href="mailto:">${StoreInfo.email}</a></p>
        </div>
    </div>
    <div class="footer-right">
        <p class="footer-company-about">
            <span>Giới thiệu về chúng tôi</span><br />
            <span>${StoreInfo.about}</span>

        <div>
            <a href="" style="color: white" class="facebook">
                <img width="30" height="30" src="resources/image/facebook_icon.jpg" />
                
            </a>
        </div>
    </div>
</footer>


