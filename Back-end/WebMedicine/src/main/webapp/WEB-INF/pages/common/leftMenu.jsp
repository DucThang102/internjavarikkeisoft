<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="col-md-3 col-sm-12 col-xs-12">

	<div class="catagory hidden-sm">
		<div class="sidebar-title">
			<h3>Danh mục thuốc</h3>
		</div>
		<ul class="menu-sidebar">

			<c:forEach var="category" items="${categories}">
				<li><a href="#"> ${category.name}</a></li>
			</c:forEach>

		</ul>
	</div>

	<div class="contact">
		<div class="sidebar-title">
			<h3>Hỗ trợ trực tuyến</h3>
		</div>
		<div class="contact-content">
			<div class="assistants">

				<div style="float: left; padding-top: 5px;">
					<div class="col-md-2 col-sm-2 col-xs-2">
						<img style="margin-top: 10px" class="call"
							src="resources/image/call-red.png" alt="">
					</div>
					<div class="col-md-10 col-sm-10 col-xs-10">
						<span><b>Tư vấn kỹ thuật</b></span><br> <span class="name"><b>${StoreInfo.phoneNumOwner1}</b></span>
						<span class="phone"><a href="">${StoreInfo.phoneNum1}</a></span><br>

					</div>
				</div>
				<div style="float: left; padding-top: 5px;">
					<div class="col-md-2 col-sm-2 col-xs-2">
						<img style="margin-top: 10px" class="call"
							src="resources/image/call-red.png" alt="">
					</div>
					<div class="col-md-10 col-sm-10 col-xs-10">
						<span><b>tư vấn bán hàng</b></span><br> <span class="name"><b>${StoreInfo.phoneNumOwner2}</b></span>
						<span class="phone"><a href="">${StoreInfo.phoneNum2}</a></span><br>
					</div>
				</div>
				<div style="float: left; padding-top: 5px;">
					<div class="col-md-2 col-sm-2 col-xs-2">
						<img style="margin-top: 10px" class="call"
							src="resources/image/email_red.png" alt="">
					</div>
					<div class="col-md-10 col-sm-10 col-xs-10">
						<span><b>Email liên hệ</b></span><br> <span class="email"><a
							href="mailto:@ThongTinWeb.Email">${StoreInfo.email}</a></span><br>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>