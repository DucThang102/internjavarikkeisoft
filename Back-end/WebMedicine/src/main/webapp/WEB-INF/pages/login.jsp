<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Đăng nhập</title>
<!-- plugins:css -->
<link href="resources/css/materialdesignicons.min.css" rel="stylesheet" />
<link href="resources/css/vendor.bundle.base.css" rel="stylesheet" />
<link href="resources/css/vendor.bundle.addons.css" rel="stylesheet" />
<link href="resources/css/style_login.css" rel="stylesheet" />
<link rel="shortcut icon" href="resources/image/logo.jpg" />

</head>
<body>
	<div class="container-scroller">
		<div
			class="container-fluid page-body-wrapper full-page-wrapper auth-page">
			<div
				class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
				<div class="row w-100">
					<div class="col-lg-4 mx-auto">
						<div class="auto-form-wrapper">
							<form action="login" method="post">

							
								<span style="color: red;">${status}</span>
								<div class="form-group">
									<label class="label">Email</label>
									<div class="input-group">
										<input type="text" name="email"
											value="phamducthang10298@gmail.com" class="form-control"
											placeholder="Email">
										<div class="input-group-append">
											<span class="input-group-text"> <i
												class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="label">Mật khẩu</label>
									<div class="input-group">
										<input type="password" name="password" value="1111"
											class="form-control" placeholder="*********">
										<div class="input-group-append">
											<span class="input-group-text"> <i
												class="mdi mdi-check-circle-outline"></i>
											</span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<button type="submit"
										class="btn btn-primary submit-btn btn-block">Đăng
										nhập</button>
								</div>
								<div class="form-group d-flex justify-content-between">
									<a href="forgot-password" style="color: black; font-size: 12px">Quên
										mật khẩu</a>
								</div>

								<div class="text-block text-center my-3">
									<span class="text-small font-weight-semibold">Chưa có
										tài khoản ?</span> <a href="register-page"
										class="text-black text-small">Đăng kí tài khoản mới</a>
								</div>
							</form>
						</div>

					</div>
				</div>
			</div>
			<!-- content-wrapper ends -->
		</div>
		<!-- page-body-wrapper ends -->
	</div>
	<!-- container-scroller -->
	<script src="resources/js/jquery-3.3.1.min.js"></script>
	<!-- plugins:js -->
	<script src="resources/js/vendor.bundle.addons.js"></script>
	<script src="resources/js/vendor.bundle.base.js"></script>
	<!-- endinject -->
	<!-- inject:js -->
	<script src="resources/js/off-canvas.js"></script>
	<script src="resources/js/misc.js"></script>
	<!-- endinject -->
</body>
</html>