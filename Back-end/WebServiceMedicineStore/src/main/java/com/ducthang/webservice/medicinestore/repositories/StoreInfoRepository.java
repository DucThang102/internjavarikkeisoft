package com.ducthang.webservice.medicinestore.repositories;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.medicinestore.entities.StoreInfo;
import com.ducthang.webservice.medicinestore.repositoryImpl.IStoreInfo;

@Repository
@Transactional
public class StoreInfoRepository implements IStoreInfo {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public StoreInfo getStoreInfo() {
		Session session = sessionFactory.getCurrentSession();
		StoreInfo info = (StoreInfo) session.createQuery("From StoreInfo where id = 1").getSingleResult();
		return info;
	}

	@Override
	public boolean updateStoreInfo(StoreInfo info) {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.update(info);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
