package com.ducthang.webservice.medicinestore.entities;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class OrderDetailId implements Serializable {

	private static final long serialVersionUID = -7009984925877736680L;

	int order_id;

	int product_id;

	public int getOrder_id() {
		return order_id;
	}

	public void setOrder_id(int order_id) {
		this.order_id = order_id;
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int product_id) {
		this.product_id = product_id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + order_id;
		result = prime * result + product_id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderDetailId other = (OrderDetailId) obj;
		if (order_id != other.order_id)
			return false;
		if (product_id != other.product_id)
			return false;
		return true;
	}

}