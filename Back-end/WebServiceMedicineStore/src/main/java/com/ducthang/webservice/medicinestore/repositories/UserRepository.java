package com.ducthang.webservice.medicinestore.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.medicinestore.entities.User;
import com.ducthang.webservice.medicinestore.repositoryImpl.IUser;

@Repository
@Transactional
public class UserRepository implements IUser {

	@Autowired
	private SessionFactory sessionFactory;

	public User login(String email, String password) {
		Session session = sessionFactory.getCurrentSession();
		try {
			String hql = "From User where email = '" + email + "' and password = '" + password + "'";
			System.out.println(hql);
			User user = (User) session.createQuery(hql).getSingleResult();
			if (user != null) {
				return user;
			} else {
				return null;
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String register(User user) {
		Session session = sessionFactory.getCurrentSession();

		if (isEmailExisted(user.getEmail())) {
			return "Email đã được đăng ký trước đó!";
		} else {
			int status = (int) session.save(user);
			if (status > 0) {
				return "Thành công!";
			}
			return "Thất bại!";
		}
	}

	public boolean isEmailExisted(String email) {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.createQuery("From User where email = :email").setParameter("email", email).getSingleResult();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public String resetPassword(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findUserByEmail(String email) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "From User U where U.email = :email";

		Query<User> query = session.createQuery(hql, User.class);
		query.setParameter("email", email);

		List<User> list = query.list();
		if (list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}

	}

	@Override
	public User findUserByResetToken(String resetToken) {
		Session session = sessionFactory.getCurrentSession();
		String hql = "From User U where U.resetToken = :resetToken";

		Query<User> query = session.createQuery(hql, User.class);
		query.setParameter("resetToken", resetToken);
		List<User> list = query.list();
		if (list.size() > 0) {
			System.out.println(list.get(0).toString());
			return list.get(0);
		} else {
			return null;
		}

	}

	@Override
	public void updateUser(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
	}

}
