package com.ducthang.webservice.medicinestore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.medicinestore.entities.Category;
import com.ducthang.webservice.medicinestore.repositories.CategoryRepository;
import com.ducthang.webservice.medicinestore.repositoryImpl.ICategory;

@Service
public class CategoryService implements ICategory {

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> listCategories() {
		return categoryRepository.listCategories();
	}

	@Override
	public Category addCategory(Category category) {
		return categoryRepository.addCategory(category);
	}

	@Override
	public boolean removeCategory(int id) {
		return categoryRepository.removeCategory(id);
	}

	@Override
	public boolean updateCategory(Category category) {
		return categoryRepository.updateCategory(category);
	}

    @Override
    public Category getCategoryById(int id) {
        return categoryRepository.getCategoryById(id);
    }

	@Override
	public List<Category> listCategories(int page) {
		return categoryRepository.listCategories(page);
	}

	@Override
	public Long getTotalCategories() {
		return categoryRepository.getTotalCategories();
	}

}
