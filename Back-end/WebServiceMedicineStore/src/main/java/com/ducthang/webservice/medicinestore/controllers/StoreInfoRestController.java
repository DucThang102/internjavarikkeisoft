package com.ducthang.webservice.medicinestore.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.medicinestore.entities.StoreInfo;
import com.ducthang.webservice.medicinestore.repositoryImpl.IStoreInfo;
import com.ducthang.webservice.medicinestore.services.StoreInfoService;

@RestController
@RequestMapping("/store-info")
@CrossOrigin(origins = "*")
public class StoreInfoRestController implements IStoreInfo {

	@Autowired
	private StoreInfoService service;

	@Override
	@GetMapping(value = "/get", produces = MediaType.APPLICATION_JSON_VALUE)
	public StoreInfo getStoreInfo() {
		return service.getStoreInfo();
	}

	@Override
	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean updateStoreInfo(@RequestBody StoreInfo info) {
		return service.updateStoreInfo(info);
	}

}
