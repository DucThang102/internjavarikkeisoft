package com.ducthang.webservice.medicinestore.controllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/file")
@CrossOrigin(origins = "*")
public class FileRestController {

    public static String UPLOAD_DIR = "uploads";

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadFile(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        try {
            String fileName = file.getOriginalFilename();
            LocalDateTime dateTime = LocalDateTime.now();
            String dateTimeNow = DateTimeFormatter.ofPattern("ddMMYYY_HHmmss").format(dateTime);
            fileName = fileName + "_" + dateTimeNow;
            String path = request.getServletContext().getRealPath("") + UPLOAD_DIR + File.separator + fileName;
            saveFile(file, path);
            return request.getHeader("host") + File.separator + UPLOAD_DIR + File.separator + fileName;
        } catch (Exception e) {
            e.printStackTrace();
            return "Error";
        }
    }

    public void saveFile(MultipartFile file, String path) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(path));
            fileOutputStream.write(file.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
