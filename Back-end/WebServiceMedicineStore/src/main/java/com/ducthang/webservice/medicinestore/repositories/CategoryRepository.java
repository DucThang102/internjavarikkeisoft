package com.ducthang.webservice.medicinestore.repositories;

import java.util.List;

import javax.transaction.Transactional;

import com.ducthang.webservice.medicinestore.entities.Product;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.medicinestore.entities.Category;
import com.ducthang.webservice.medicinestore.repositoryImpl.ICategory;

@Repository
@Transactional
public class CategoryRepository implements ICategory {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public List<Category> listCategories() {
        Session session = sessionFactory.getCurrentSession();
        @SuppressWarnings("unchecked")
        List<Category> list = session.createQuery("From Category").list();
        return list;
    }

    @Override
    public Category addCategory(Category category) {
        Session session = sessionFactory.getCurrentSession();
        int id = (int) session.save(category);
        if (id > 1) {
            return category;
        } else {
            return null;
        }
    }

    @Override
    public boolean removeCategory(int id) {
        Session session = sessionFactory.getCurrentSession();
        Category category = session.get(Category.class, id);
        if (category != null) {
            session.delete(category);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateCategory(Category category) {
        Session session = sessionFactory.getCurrentSession();
        Category categoryPresent = session.get(Category.class, category.getId());
        if (categoryPresent != null) {
            categoryPresent.setName(category.getName());
            session.save(categoryPresent);
            return true;
        }
        return false;
    }

    @Override
    public Category getCategoryById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return session.get(Category.class, id);
    }

    @Override
    public List<Category> listCategories(int page) {
        int itemsPerPage = 5;
        int offset = (page == 1) ? 0 : (page -1) * itemsPerPage;

        Session session = sessionFactory.getCurrentSession();
        String hql = "From Category";
        @SuppressWarnings("unchecked")
        Query<Category> query = (Query<Category>) session.createQuery(hql).setFirstResult(offset).setMaxResults(itemsPerPage);
        return query.list();
    }

	@Override
	public Long getTotalCategories() {
		Session session = sessionFactory.getCurrentSession();
		@SuppressWarnings("deprecation")
		Criteria criteria = session.createCriteria(Category.class).setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

}
