package com.ducthang.webservice.medicinestore.repositoryImpl;

import com.ducthang.webservice.medicinestore.entities.User;

public interface IUser {
	public User login(String email, String password);
	
	public String register(User user);
	
	public String resetPassword(String email);
	
	public User findUserByEmail(String email);

	public User findUserByResetToken(String resetToken);

	public void updateUser(User user);
		
}
