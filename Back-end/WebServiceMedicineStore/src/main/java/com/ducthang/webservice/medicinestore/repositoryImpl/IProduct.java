package com.ducthang.webservice.medicinestore.repositoryImpl;

import java.util.List;

import com.ducthang.webservice.medicinestore.entities.Product;

public interface IProduct {
	public List<Product> listProduct(int offset, int limit);
	
	public Product getProductById(int id);
	
	public boolean addProduct(Product product);
	
	public boolean removeProduct(int id);
	
	public boolean updateProduct(Product product);
	
	public Long getTotalProducts();
}
