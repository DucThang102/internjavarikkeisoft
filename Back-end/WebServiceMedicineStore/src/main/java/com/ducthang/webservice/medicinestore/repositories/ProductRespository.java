package com.ducthang.webservice.medicinestore.repositories;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ducthang.webservice.medicinestore.entities.Product;
import com.ducthang.webservice.medicinestore.repositoryImpl.IProduct;

@Repository
@Transactional
public class ProductRespository implements IProduct {

	@Autowired
	private SessionFactory sesssionFactory;

	@Override
	public List<Product> listProduct(int offset, int limit) {
		Session session = sesssionFactory.getCurrentSession();
		String hql = "From Product";
		@SuppressWarnings("unchecked")
		Query<Product> query = (Query<Product>) session.createQuery(hql).setFirstResult(offset).setMaxResults(limit);
		return query.list();
	}

	@Override
	public Product getProductById(int id) {
		Session session = sesssionFactory.getCurrentSession();
		return session.get(Product.class, id);
	}

	// TODO
	@Override
	public boolean addProduct(Product product) {
		Session session = sesssionFactory.getCurrentSession();
		int id = (int) session.save(product);
		if (id > 1) {
			return true;
		}
		return false;
	}

	// TODO constraint
	@Override
	public boolean removeProduct(int id) {
		Session session = sesssionFactory.getCurrentSession();
		Product product = session.get(Product.class, id);
		if (product != null) {
			session.delete(product);
			return true;
		}
		return false;
	}

	// TODO
	@Override
	public boolean updateProduct(Product product) {
		try {
			Session session = sesssionFactory.getCurrentSession();
			session.update(product);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Long getTotalProducts() {
		Session session = sesssionFactory.getCurrentSession();
		@SuppressWarnings("deprecation")
		Criteria criteria = session.createCriteria(Product.class).setProjection(Projections.rowCount());
		return (Long) criteria.uniqueResult();
	}

}
