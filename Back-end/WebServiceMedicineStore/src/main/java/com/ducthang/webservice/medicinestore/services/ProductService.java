package com.ducthang.webservice.medicinestore.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.medicinestore.entities.Product;
import com.ducthang.webservice.medicinestore.repositories.ProductRespository;
import com.ducthang.webservice.medicinestore.repositoryImpl.IProduct;

@Service
public class ProductService implements IProduct{
	
	@Autowired
	private ProductRespository repository;

	@Override
	public List<Product> listProduct(int offset, int limit) {
		return repository.listProduct(offset, limit);
	}

	@Override
	public Product getProductById(int id) {
		return repository.getProductById(id);
	}

	@Override
	public boolean addProduct(Product product) {
		return repository.addProduct(product);
	}

	@Override
	public boolean removeProduct(int id) {
		return repository.removeProduct(id);
	}

	@Override
	public boolean updateProduct(Product product) {
		return repository.updateProduct(product);
	}

	@Override
	public Long getTotalProducts() {
		return repository.getTotalProducts();
	}

}
