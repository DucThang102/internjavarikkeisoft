package com.ducthang.webservice.medicinestore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.medicinestore.entities.User;
import com.ducthang.webservice.medicinestore.repositories.UserRepository;
import com.ducthang.webservice.medicinestore.repositoryImpl.IUser;

@Service
public class UserService implements IUser{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User login(String email, String password) {
		return userRepository.login(email, password);
	}

	@Override
	public String register(User user) {
		return userRepository.register(user);
	}

	@Override
	public String resetPassword(String email) {
		return userRepository.resetPassword(email);
	}

	@Override
	public User findUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}

	@Override
	public User findUserByResetToken(String resetToken) {
		return userRepository.findUserByResetToken(resetToken);
	}

	@Override
	public void updateUser(User user) {
		userRepository.updateUser(user);
	}

}
