package com.ducthang.webservice.medicinestore.entities;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "order_detail")
public class OrderDetail {

	@EmbeddedId
	private OrderDetailId OrderDetail = new OrderDetailId();

	@Column(name = "amount")
	private int amount;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
