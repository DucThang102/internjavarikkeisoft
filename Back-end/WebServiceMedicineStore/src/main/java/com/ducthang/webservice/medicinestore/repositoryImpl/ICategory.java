package com.ducthang.webservice.medicinestore.repositoryImpl;

import java.util.List;

import com.ducthang.webservice.medicinestore.entities.Category;

public interface ICategory {
	public List<Category> listCategories();
	
	public Category addCategory(Category category);
	
	public boolean removeCategory(int id);
	
	public boolean updateCategory(Category category);

	public Category getCategoryById(int id);

	public List<Category> listCategories(int page);

	public Long getTotalCategories();

}
