package com.ducthang.webservice.medicinestore.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "store_info")
public class StoreInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "address")
	private String address;

	@Column(name = "email")
	private String email;

	@Column(name = "facebook")
	private String facebook;

	@Column(name = "about")
	private String about;

	@Column(name = "phone_num_1")
	private int phoneNum1;

	@Column(name = "phone_num_owner_1")
	private String phoneNumOwner1;

	@Column(name = "phone_num_2")
	private int phoneNum2;

	@Column(name = "phone_num_owner_2")
	private String phoneNumOwner2;

	@Column(name = "latitude")
	private int latitude;

	@Column(name = "longitude")
	private String longitude;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		this.facebook = facebook;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public int getPhoneNum1() {
		return phoneNum1;
	}

	public void setPhoneNum1(int phoneNum1) {
		this.phoneNum1 = phoneNum1;
	}

	public String getPhoneNumOwner1() {
		return phoneNumOwner1;
	}

	public void setPhoneNumOwner1(String phoneNumOwner1) {
		this.phoneNumOwner1 = phoneNumOwner1;
	}

	public int getPhoneNum2() {
		return phoneNum2;
	}

	public void setPhoneNum2(int phoneNum2) {
		this.phoneNum2 = phoneNum2;
	}

	public String getPhoneNumOwner2() {
		return phoneNumOwner2;
	}

	public void setPhoneNumOwner2(String phoneNumOwner2) {
		this.phoneNumOwner2 = phoneNumOwner2;
	}

	public int getLatitude() {
		return latitude;
	}

	public void setLatitude(int latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}