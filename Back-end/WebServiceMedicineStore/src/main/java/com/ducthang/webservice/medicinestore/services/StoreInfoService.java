package com.ducthang.webservice.medicinestore.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ducthang.webservice.medicinestore.entities.StoreInfo;
import com.ducthang.webservice.medicinestore.repositories.StoreInfoRepository;
import com.ducthang.webservice.medicinestore.repositoryImpl.IStoreInfo;

@Service
public class StoreInfoService implements IStoreInfo {

	@Autowired
	private StoreInfoRepository repository;
	
	@Override
	public StoreInfo getStoreInfo() {
		return repository.getStoreInfo();
	}

	@Override
	public boolean updateStoreInfo(StoreInfo info) {
		return repository.updateStoreInfo(info);
	}

}
