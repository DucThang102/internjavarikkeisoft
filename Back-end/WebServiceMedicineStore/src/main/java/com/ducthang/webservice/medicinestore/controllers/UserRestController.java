package com.ducthang.webservice.medicinestore.controllers;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.medicinestore.entities.User;
import com.ducthang.webservice.medicinestore.services.EmailService;
import com.ducthang.webservice.medicinestore.services.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserRestController {

	@Autowired
	private UserService userService;

	@Autowired
	private EmailService emailService;

	@GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@RequestMapping("/login")
	public User login(@RequestParam("email") String email, @RequestParam("password") String password) {

		User user = userService.login(email, password);
		if (user != null) {
			return user;
		}
		return null;
	}

	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@RequestMapping("/register")
	@ResponseBody
	public String register(@RequestBody User user) {
		System.out.println(user.toString());
		return userService.register(user);
	}

	@GetMapping(value = "/forgotPassword")
	@ResponseBody
	public String forgotPassword(@RequestParam("email") String email, HttpServletRequest request) {
		User user = userService.findUserByEmail(email);
		if (user == null) {
			return "Không tìm thấy tài khoản nào có email này!";
		} else {
			// Generate random 36-character string token for reset password
			user.setResetToken(UUID.randomUUID().toString());

			userService.updateUser(user);

			// Email message
			SimpleMailMessage passwordResetEmail = new SimpleMailMessage();
			passwordResetEmail.setTo(user.getEmail());
			passwordResetEmail.setSubject("Yêu cầu khôi phục mật khẩu");
			passwordResetEmail.setText("Chúng tôi nhận được yêu cầu khôi phục mật khẩu từ bạn.\n"
					+ "Đây là mã để khôi phục mật khẩu của bạn: " + user.getResetToken()
					+ "\nVui lòng không cung cấp cho bất kỳ ai để bảo vệ tài khoản của bạn.");

			emailService.sendEmail(passwordResetEmail);

			return "Chúng tôi đã gửi mail khôi phục mật khẩu đến " + user.getEmail() + ". Xin vui lòng kiểm tra email!";
		}
	}

	@GetMapping(value = "/checkResetToken")
	@ResponseBody
	public boolean isResetTokenValid(@RequestParam("resetToken") String resetToken) {
		User user = userService.findUserByResetToken(resetToken);
		if (user != null) {
			return true;
		}
		return false;
	}

	@GetMapping(value = "/resetPassword")
	@ResponseBody
	public String resetPassword(@RequestParam("resetToken") String resetToken,
			@RequestParam("newPassword") String newPassword) {
		User user = userService.findUserByResetToken(resetToken);
		if (user != null) {
			user.setPassword(newPassword);
			user.setResetToken(null);
			userService.updateUser(user);
			return "Thành công !";
		} else {
			return "Mã token không chính xác !";
		}
	}

}
