package com.ducthang.webservice.medicinestore.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.medicinestore.entities.Category;
import com.ducthang.webservice.medicinestore.repositoryImpl.ICategory;
import com.ducthang.webservice.medicinestore.services.CategoryService;

@RestController
@RequestMapping("/category")
@CrossOrigin(origins = "*")
public class CategoryRestController implements ICategory {

    @Autowired
    private CategoryService service;

    @Override
    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> listCategories() {
        return service.listCategories();
    }

    @Override
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Category addCategory(@RequestBody Category category) {
        return service.addCategory(category);
    }

    @Override
    @DeleteMapping(value = "/delete")
    @ResponseBody
    public boolean removeCategory(@RequestParam("id") int id) {
        return service.removeCategory(id);
    }

    @Override
    @PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean updateCategory(@RequestBody Category category) {
        return service.updateCategory(category);
    }

    @Override
    @GetMapping(value = "/getCategoryById", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Category getCategoryById(@RequestParam("id") int id) {
        return service.getCategoryById(id);
    }

    @Override
    @GetMapping(value = "/getCategoryByPageNumber", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Category> listCategories(@RequestParam("page") int page) {
        return service.listCategories(page);
    }

    @Override
    @GetMapping(value = "/getTotalCategories")
    @ResponseBody
    public Long getTotalCategories() {
        return service.getTotalCategories();
    }


}
