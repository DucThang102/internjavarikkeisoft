package com.ducthang.webservice.medicinestore.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ducthang.webservice.medicinestore.entities.Product;
import com.ducthang.webservice.medicinestore.repositoryImpl.IProduct;
import com.ducthang.webservice.medicinestore.services.ProductService;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*")
public class ProductRestController implements IProduct {

	@Autowired
	private ProductService service;

	@Override
	@GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Product> listProduct(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
		return service.listProduct(offset, limit);
	}

	@Override
	@GetMapping(value = "/getProductById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public Product getProductById(@RequestParam("id") int id) {
		return service.getProductById(id);
	}

	@Override
	@PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public boolean addProduct(@RequestBody Product product) {
		return service.addProduct(product);
	}

	@Override
	@DeleteMapping(value = "/delete")
	@ResponseBody
	public boolean removeProduct(@RequestParam("id") int id) {
		return service.removeProduct(id);
	}

	@Override
	@PutMapping(value = "/update")
	@ResponseBody
	public boolean updateProduct(@RequestBody Product product) {
		return service.updateProduct(product);
	}

	@Override
	@GetMapping(value = "totalProducts")
	@ResponseBody
	public Long getTotalProducts() {
		return service.getTotalProducts();
	}

}
