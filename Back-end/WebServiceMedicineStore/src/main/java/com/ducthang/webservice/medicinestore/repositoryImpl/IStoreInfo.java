package com.ducthang.webservice.medicinestore.repositoryImpl;

import com.ducthang.webservice.medicinestore.entities.StoreInfo;

public interface IStoreInfo {
	public StoreInfo getStoreInfo();
	
	public boolean updateStoreInfo(StoreInfo info);
}
